// ==UserScript==
// @name        WK Community Mnemonics
// @namespace   wkcm
// @description This script allows WaniKani members to contribute their own mnemonics which appear on any page that includes item info.
// @require     https://rawgit.com/jsoma/tabletop/master/src/tabletop.js
// @require     https://rawgit.com/jackmoore/autosize/v1/jquery.autosize.js
// @exclude     *.wanikani.com
// @exclude     *.wanikani.com/level/radicals*
// @include     *.wanikani.com/level/*
// @include     *.wanikani.com/kanji*
// @include     *.wanikani.com/vocabulary*
// @include     *.wanikani.com/review/session
// @include     *.wanikani.com/lesson/session
// @version     0.9.7.8
// @author      Samuel H
// @grant       none
/* This script is licensed under the Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) license
 * Details: http://creativecommons.org/licenses/by-nc/4.0/ */
/* jshint varstmt: false, laxbreak: true */
/* globals $, event, confirm, alert, localStorage, xhr, Tabletop, MutationObserver, CMVersionCheck: true, CMIndex: true, CMData: true, CMInitData: true, CMIndex: true, CMTableData: true, CMSettings: true, CMVotes: true, CMPageIndex: true, CMSortMap: true, CMPageMap: true, CMInitVotes: true, CMPostReady: true, CMUser: true, CMTableItems: true, CMInitSettings: true, CMSelTemp: true, CMLastTag: true */

/** @enum {number} */
var ListType = {
	none: 0,
	level: 1,
	kanji: 2,
	vocabulary: 3
};

function CMGetInfo() {
	var isReview = window.location.pathname.indexOf("/review/") > -1;
	var isLesson = window.location.pathname.indexOf("/lesson/") > -1;
	var afterDomainPath = window.location.pathname.slice(window.location.pathname.indexOf("com/") + 2);
	var isList = !isReview && !isLesson && new RegExp("(level/[0-9]{1,2}|(kanji|vocabulary)(.difficulty=[A-Z]+)?)$", "i").test(afterDomainPath);
	var listType = ListType.none;
	if (isList) {
		if (afterDomainPath.indexOf("level") > -1) { listType = ListType.level; }
		else if (afterDomainPath.indexOf("kanji") > -1) { listType = ListType.kanji; }
		else if (afterDomainPath.indexOf("vocabulary") > -1) { listType = ListType.vocabulary; }
	}
	var level = null;
	if (listType === ListType.level) {
		level = Number(afterDomainPath.slice(afterDomainPath.lastIndexOf("/") + 1));
	}
	/** @type {?string} */
	var user = null;
	var possibleUserLinks = $(".account a").filter(function() { return this.href.indexOf("/users/") !== -1; });
	if (possibleUserLinks.length === 1) {
		user = possibleUserLinks[0].href.substring(possibleUserLinks[0].href.lastIndexOf("/") + 1);
	}
	return {
		version: "1.0.0",
		isReview: isReview,
		isLesson: isLesson,
		isReviewOrLesson: function() { return this.isReview || this.isLesson; },
		listType: listType,
		level: level, /* Only available if listType is `level` */
		isList: function() { return this.listType !== ListType.none; },
		isChrome: (navigator.userAgent.toLowerCase().indexOf('chrome') > -1),
		item: new ItemInfo(null, "", [], [], [], []),
		ready: false,
		preloadReady: false,
		stylesAdded: false,
		user: user
	};
}

/**
 * @param {string} text - The main text of the mnemonic
 * @param {string} author - The person who created the mnemonic
 * @param {number} votes - The total votes given to this mnemonic
 */
function Mnemonic(text, author, votes) {
	this.text = text;
	this.author = author;
	this.votes = votes;
}

/**
 * @returns {(Node | string)[]}
 */
Mnemonic.prototype.createHTML = function() {
	var authorText = makeWKUserLink(this.author);

	return [sanitizeMnemonic(this.text), br(), "by ", authorText];
}

/**
 * @param {ItemType} type - The item's type
 * @param {string} char - The item's word
 * @param {Array<Mnemonic>} reading - Mnemonics for the reading of an item
 * @param {Array<Mnemonic>} meaning - Mnemonics for the meaning of an item
 * @param {Array<string>} readingRequests - People who have requested the reading of this item
 * @param {Array<string>} meaningRequests - People who have requested the meaning of this item
 */
function ItemInfo(type, char, reading, meaning, readingRequests, meaningRequests) {
	this.type = type;
	this.char = char;
	this.reading = reading;
	this.meaning = meaning;
	this.readingRequests = readingRequests;
	this.meaningRequests = meaningRequests;
}
ItemInfo.prototype.name = function() {
	return itemTypeShortName(this.type) + this.char;
};
ItemInfo.prototype.hasMnemonics = function() {
	return this.reading.length !== 0 || this.meaning.length !== 0;
};
ItemInfo.prototype.isRequested = function() {
	return this.readingRequests.length !== 0 || this.meaningRequests.length !== 0;
};
ItemInfo.prototype.readingInfo = function() {
	return {mnemonics: this.reading, requests: this.readingRequests};
};
ItemInfo.prototype.meaningInfo = function() {
	return {mnemonics: this.meaning, requests: this.meaningRequests};
};
/**
 * @typedef {object} SpreadsheetRow
 * @property {string} item
 * @property {string} meaningmnem
 * @property {string} readingmnem
 * @property {string} meaningscore
 * @property {string} readingscore
 * @property {string} meaninguser
 * @property {string} readinguser
 * @property {string} info
 * @property {string} lastupdated
 */
/** @enum {string} */
var ItemType = {
	radical: "r",
	kanji: "k",
	vocabulary: "v"
};
/** @enum {string} */
var MnemonicType = {
	reading: "r",
	meaning: "m"
};
var CMInfo = CMGetInfo();
window.CMInfo = CMInfo;
// If we fail to load the user with CMGetInfo (will happen in reviews and lessons), we may be able to get it off of window.WaniKani
// Note: The double document.ready / window.load is because WaniKani sets the variable in a document.ready and we want to make sure we execute after that
$(document).ready(function() {
	$(window).load(function() {
		if (CMInfo.user) { return; }
		if (!window.WaniKani) { return; }
		if (window.WaniKani.username) {
			CMInfo.user = window.WaniKani.username;
		}
		else if (window.WaniKani.studyInformation
		      && window.WaniKani.studyInformation.user_information
		      && window.WaniKani.studyInformation.user_information.username) {
			CMInfo.user = window.WaniKani.studyInformation.user_information.username;
		}
	});
});

var CMMainData = {
	/** @type {Date | undefined} */
	lastUpdate: undefined,
	/** @type {Object.<string, ItemInfo>} */
	items: {}
};
window.CMMainData = CMMainData;

var CMSaveData = {
	/** @type {Object.<string, {reading: ?string, meaning: ?string}>} Maps mnemonics to authors */
	selectedMnemonics: {}
};
window.CMSaveData = CMSaveData;

var public_spreadsheet_url = 'https://docs.google.com/spreadsheet/pub?hl=en_US&hl=en_US&key=1sXSNlOITCaNbXa4bUQSfk_5Uvja6qL3Wva8bPv-3B2o&output=html';
var CMMeaningElement = '<h2 class="cm">Community Meaning Mnemonic</h2><section class="cm cm-meaning"><p class="loadingCM">Loading...</p></section>';
var CMReadingElement = '<h2 class="cm">Community Reading Mnemonic</h2><section class="cm cm-reading"><p class="loadingCM">Loading...</p></section>';
if (CMInfo.isReviewOrLesson()) {
	$(document).ready(function() {
		var checkContentLoaded = setInterval(function() {
			if (($("#character span").html() !== "") || ($("#character").html() !== "" && $("#character").html() !== "&nbsp;")) {
				clearInterval(checkContentLoaded);
				init();
			}
		}, 1000);
	});
} else {
	$(document).ready(init);
}
function loadCacheData() {
	var string = localStorage.getItem("CMCache");
	if (!string) { return; }
	var deserialized = JSON.parse(string);
	if (!deserialized || !deserialized.lastSpreadsheetCheck || !deserialized.items) { return; }
	CMMainData.lastUpdate = new Date(deserialized.lastSpreadsheetCheck);
	CMMainData.items = compactMapValues(deserialized.items, function(key, entry) {
		return new ItemInfo(
			/* type */ itemTypeFromString(key.substring(0, 1)),
			/* char */ key.substring(1),
			/* reading */ (entry.r || []).map(function(item) { return new Mnemonic(item.t, item.a, item.v); }),
			/* meaning */ (entry.m || []).map(function(item) { return new Mnemonic(item.t, item.a, item.v); }),
			/* readingRequests */ entry.R || [],
			/* meaningRequests */ entry.M || []
		);
	});
}
function loadSaveData() {
	var string = localStorage.getItem("CMSave");
	if (!string) { return; }
	var deserialized = JSON.parse(string);
	if (!deserialized || !deserialized.selectedMnemonics) { return; }
	CMSaveData.selectedMnemonics = compactMapValues(deserialized.selectedMnemonics, function(key, entry) {
		return {
			reading: entry.r || null,
			meaning: entry.m || null
		};
	});
}
function loadVariables() {
	loadSaveData();
	loadCacheData();
}
function storeCacheData() {
	var toSerialize = {
		lastSpreadsheetCheck: CMMainData.lastUpdate,
		items: compactMapValues(CMMainData.items, function(key, entry) {
			// Use a smaller representation for storage
			var output = {};
			var nonEmpty = false;
			if (entry.reading.length > 0) {
				output.r = entry.reading.map(function(item) { return { t: item.text, a: item.author, v: item.votes }; });
				nonEmpty = true;
			}
			if (entry.meaning.length > 0) {
				output.m = entry.meaning.map(function(item) { return { t: item.text, a: item.author, v: item.votes }; });
				nonEmpty = true;
			}
			if (entry.readingRequests.length > 0) {
				output.R = entry.readingRequests;
				nonEmpty = true;
			}
			if (entry.meaningRequests.length > 0) {
				output.M = entry.meaningRequests;
				nonEmpty = true;
			}
			return nonEmpty ? output : null;
		})
	};
	var serialized = JSON.stringify(toSerialize);
	localStorage.setItem("CMCache", serialized);
}
function storeSaveData() {
	var toSerialize = {
		selectedMnemonics: compactMapValues(CMSaveData.selectedMnemonics, function(key, entry) {
			// Use a smaller representation for storage
			var output = {};
			if (entry.reading) {
				output.r = entry.reading;
			}
			if (entry.meaning) {
				output.m = entry.meaning;
			}
			return output;
		})
	};
	// Don't overwrite the current save if nothing is loaded
	if (Object.keys(toSerialize.selectedMnemonics).length > 0) {
		var serialized = JSON.stringify(toSerialize);
		localStorage.setItem("CMSave", serialized);
	}
}
function saveVariables() {
	storeCacheData();
	storeSaveData();
}

function init() {
	loadVariables();
	addCMStyles();
	addContainers();
	var quizInfo = document.getElementById("item-info-col2");
	if (quizInfo) {
		var lessonQuizAdder = new MutationObserver(function() {
			$("#note-reading").parent().children(".cm-reading, #note-reading + h2").css("display", $("#note-reading").css("display"));
			$("#note-meaning").parent().children(".cm-meaning, #note-meaning + h2").css("display", $("#note-meaning").css("display"));
			var reading = document.getElementById("note-reading");
			var meaning = document.getElementById("note-meaning");
			if (reading) { readingNoteHider.observe(reading, { attributes: true }); }
			if (meaning) { meaningNoteHider.observe(meaning, { attributes: true }); }
			if ($("#item-info-col2 .cm").length === 0) {
				addContainers();
				displayInfo();
			}
		});
		var readingNoteHider = new MutationObserver(function() {
			$("#note-reading").parent().children(".cm-reading, #note-reading + h2").css("display", $("#note-reading").css("display"));
		});
		var meaningNoteHider = new MutationObserver(function() {
			$("#note-meaning").parent().children(".cm-meaning, #note-meaning + h2").css("display", $("#note-meaning").css("display"));
		});
		lessonQuizAdder.observe(quizInfo, { childList: true });
	}

	var character = document.getElementById("character");
	if (character) {
		var characterUpdater = new MutationObserver(displayInfo);
		characterUpdater.observe(character, { childList: true, subtree: true });
	}
	//Start Code Credit: jsoma from Github
	try {
		Tabletop.init( { key: public_spreadsheet_url,
		                 callback: showInfo,
		                 prettyColumnNames: false,
		                 simpleSheet: true } );
	} catch(e) {
		if (!CMInfo.isReviewOrLesson()) {
			$(".loadingCM").html("An error occurred while trying to access the database; reload the page to try again.");
		}
		else {
			alert('The following error occurred while trying to access the database: "' + e + '". If the problem persists, make sure your internet connection is working properly.');
		}
	}
	//End Code Credit
	$(document).ready(function() {
		displayInfo();
	});
}

function addContainers() {
	$(".cm").remove();

	if (CMInfo.isList()) {
		$(".additional-info.level-list.legend li").parent().prepend(getCMLegend(true)).prepend(getCMLegend(false));
		$(".legend.level-list span.commnem").css("background-color", "#71aa00");
		$(".legend.level-list span.commnem-req").css("background-color", "#e1aa00");
	} else {
		if (CMInfo.isLesson) {
			$(CMMeaningElement).insertAfter($("#supplement-kan-meaning-notes"));
			$(CMMeaningElement).insertAfter($("#supplement-voc-meaning-notes"));
			$(CMReadingElement).insertAfter($("#supplement-kan-reading-notes"));
			$(CMReadingElement).insertAfter($("#supplement-voc-reading-notes"));
		}

		$(CMMeaningElement).insertAfter($("#note-meaning"));
		$(CMReadingElement).insertAfter($("#note-reading"));

	}
}

function initCMList() {
	function checkCMComMnemKanji() {
		checkCMComMnem(this, ItemType.kanji);
	}
	function checkCMComMnemVocab() {
		checkCMComMnem(this, ItemType.vocabulary);
	}

	switch (CMInfo.listType) {
	case ListType.level:
		$("#level-" + CMInfo.level + "-kanji .character-item").each(checkCMComMnemKanji);
		$("#level-" + CMInfo.level + "-vocabulary .character-item").each(checkCMComMnemVocab);
		break;
	case ListType.kanji:
		$(".character-item").each(checkCMComMnemKanji);
		break;
	case ListType.vocabulary:
		$(".character-item").each(checkCMComMnemVocab);
		break;
	default:
		console.warn("Used InitCMList even though ListType was " + CMInfo.listType);
	}
}

/**
 * Adds badges a WK list item if the item has mnemonics requested
 * @param {HTMLElement} element The element 
 * @param {ItemType} type 
 */
function checkCMComMnem(element, type) {
	var char = $(element).children("a").children(".character").text().trim();
	var info = findItem(type, char);
	$(element).children(".commnem-badge, .commnem-badge-req").remove();
	if (info.hasMnemonics() || info.isRequested()) {
		if ($(element).children(".recently-unlocked-badge").length > 0) {
			$(getCMBadge(true, info.isRequested(), info.type)).insertAfter($(element).children("span")).prev().css("top", "-0.5em");
		}
		else {
			$(getCMBadge(false, info.isRequested(), info.type)).insertAfter($(element).children("span"));
		}
	}
}

function getCMLegend(isReq) {
	return $('<li class="cm"><div><span class="commnem' + (isReq ? "-req" : "") + '" lang="ja">共</span></div>' + (isReq ? "Mnemonic Requested" : "Community Mnemonics") + '</li>');
}
/**
 * Make a badge for an item on one of the WK list pages
 * @param {boolean} isRecent Whether the item already has a recently unlocked badge
 * @param {boolean} isReq Whether the item has a mnemonic requested
 * @param {ItemType} type The type of item
 */
function getCMBadge(isRecent, isReq, type) {
	return $('<span lang="ja" ' + (isRecent ? ' style="top: ' + ((type === ItemType.kanji) ? '2.25em" ' : '1em" ') : '') + 'class="item-badge commnem-badge' + (isReq ? "-req" : "") + '"></span>');
}
/** @deprecated */
function initCMReview(isLessonQuiz) {
	if (isLessonQuiz) {
		CMInfo.isReview = true;
		$("#cm-meaning, #cm-reading").prev().remove();
		$("#cm-meaning, #cm-reading").remove();
		$("#supplement-nav li:nth-child(2), #supplement-nav li:nth-child(3)").off("click", clickCMLessonTab);
		$("#supplement-nav li:last-child").off("click", clickLastLessonTab);
		$("#batch-items li:not(.active)").off("click", clickCMLessonItem);
		var checkFirstItemLoaded = setInterval(function() {
			if ((!isLessonQuiz && $("#character span").html() !== "" && $("#character span").html() !== undefined) || (isLessonQuiz && $("#character").html() !== "" && $("#character").html() !== undefined)) {
				clearInterval(checkFirstItemLoaded);
				newCMReviewItem();
			}
		}, 300);
	}
	$("#user-response").next().on("click", clickCMReviewSubmit);
	$("#all-info").on("click", clickCMReviewAllInfo);
}
/** @deprecated */
function clickCMReviewSubmit() {
	if ($(this).prev().attr("disabled") === "disabled") {
		var oldChar = CMInfo.item.char;
		var oldType = CMInfo.item.type;
		var char, type;
		if (!CMInfo.isLesson) {
			char = decodeURIComponent($("#character span").html());
			type = itemTypeFromString($("#character").attr("class"));
		} else {
			char = decodeURIComponent($("#character").html());
			type = itemTypeFromString($("#main-info").attr("class"));
		}
		CMInfo.item = findItem(type, char);
		if (oldChar !== CMInfo.item.char || oldType !== CMInfo.item.type) {
			CMInfo.ready = false;
			newCMReviewItem();
		}
		var checkInfoLoaded = setInterval(function() {
			if (CMInfo.ready
			 && !$("#screen-lesson-ready").is(":visible")
			 && $("#item-info-col2").html().length > 0
			 && $("#option-item-info").hasClass("active")
			 && $("#additional-content-load").css("display") !== "block") {
				clearInterval(checkInfoLoaded);
				if ($("#cm-meaning").length === 0 && $("#cm-reading").length === 0) {
					$('<h2>Community Meaning Mnemonic</h2><section id="cm-meaning" class="cm"><p class="loadingCM">Loading...</p></section>')
						.insertAfter($("#note-meaning"));
					$('<h2>Community Reading Mnemonic</h2><section id="cm-reading" class="cm"><p class="loadingCM">Loading...</p></section>')
						.insertAfter($("#note-reading"));
					if (itemTypeSupportsMnemonics(CMInfo.type)) {
						loadCM(/* fromForm */ false, /* meaning */ false);
					}
				} else if ($("#item-info-col2 #cm-meaning").length === 0 && $("#item-info-col2 #cm-reading").length === 0) {
					var checkPreloadReady = setInterval(function() {
						if (CMInfo.preloadReady) {
							clearInterval(checkPreloadReady);
							CMInfo.ready = false;
							var meaning = $("#cm-meaning-container").detach();
							var reading = $("#cm-reading-container").detach();
							meaning.insertAfter($("#note-meaning")).children("h2, div").unwrap();
							reading.insertAfter($("#note-reading")).children("h2, div").unwrap();
							updateCMMargins(true, true);
						}
					}, 250);
				}
				if ($("#all-info").css("display") !== "none") {
					if (!$("#question-type").hasClass("meaning") || CMType === "r") {
						$("#cm-meaning").hide().prev().hide();
					}
					if (!$("#question-type").hasClass("reading") || CMType === "r") {
						$("#cm-reading").hide().prev().hide();
					}
				}
			} else if ($("#screen-lesson-ready").is(":visible") || $("#quiz").css("display") === "none") {
				clearInterval(checkInfoLoaded);
				if (CMInfo.isReview) {
					initCMLesson(/* fromLessonQuiz */ true);
				}
			} else if (CMInfo.ready && $("#cm-meaning").length === 0 && $("#cm-reading").length === 0) {
				$("#information")
					.append($('<div id="cm-meaning-container" style="display: none"><h2>Community Meaning Mnemonic</h2><section id="cm-meaning" class="cm"><p class="loadingCM">Loading...</p></section></div>'))
					.append($('<div id="cm-reading-container" style="display: none"><h2>Community Reading Mnemonic</h2><section id="cm-reading" class="cm"><p class="loadingCM">Loading...</p></section></div>'));
				if (itemTypeSupportsMnemonics(CMInfo.type)) {
					loadCM(/* fromForm */ false, /* meaning */ false);
				}
			}
		}, 500);
		$("#user-response").next().one("click", function() {
			setTimeout(function() {
				if (CMInfo.ready && !$("#screen-lesson-ready").is(":visible")) {
					clearInterval(checkInfoLoaded);
				}
			}, 500);
		});
	} else {
		setTimeout(function() {
			$("#cm-meaning-container").remove();
			$("#cm-reading-container").remove();
			CMInfo.ready = false;
			newCMReviewItem();
		}, 300);
	}
}
/** @deprecated */
function clickCMReviewAllInfo() {
	$("#note-meaning + h2").show();
	$("#note-reading + h2").show();
	updateCMMargins(true, true);
}
function newCMReviewItem() {
	CMInfo.preloadReady = false;
	var char, type;
	if (!CMInfo.isLesson) {
		char = decodeURIComponent($("#character span").html());
		type = itemTypeFromString($("#character").attr("class"));
	} else {
		char = decodeURIComponent($("#character").html());
		type = itemTypeFromString($("#main-info").attr("class"));
	}
	CMInfo.item = findItem(type, char);
	if (CMInfo.item.type !== ItemType.radical) {
		$("#cm-meaning, #note-meaning + h2").remove();
		$("#cm-reading, #note-reading + h2").remove();
		displayInfo();
	}
}
/** @deprecated */
function initCMLesson(fromLessonQuiz) {
	if (fromLessonQuiz) {
		CMIsReview = false;
		$("#user-response").next().off("click", clickCMReviewSubmit);
		$("#all-info").off("click", clickCMReviewAllInfo);
	}
	var checkLessonReady = setInterval(function() {
		if (!$("#screen-lesson-ready").is(":visible")) {
			clearInterval(checkLessonReady);
			if (fromLessonQuiz) {
				CMChar = decodeURIComponent($("#character").html());
				CMType = (($("#main-info").attr("class") !== "radical") ? (($("#main-info").attr("class") === "kanji") ? "k" : "v") : "r");
				if (CMType !== "r") {
					$('<h2>Community Meaning Mnemonic</h2><div id="cm-meaning" class="cm"><p class="loadingCM">Loading...</p></div>')
						.insertAfter($("#supplement-" + ((CMType === "k") ? "kan" : "voc") + "-meaning-notes"));
					$('<h2>Community Reading Mnemonic</h2><div id="cm-reading" class="cm"><p class="loadingCM">Loading...</p></div>')
						.insertAfter($("#supplement-" + ((CMType === "k") ? "kan" : "voc") + "-reading-notes"));
				}
				if (CMData[CMType][CMChar] === undefined) {
					CMIndex = Object.keys(CMData.k).length + Object.keys(CMData.v).length + 2;
					CMSettings[CMType][CMChar] = {"m": {"p": "", "c": false}, "r": {"p": "", "c": false}};
					CMVotes[CMType][CMChar] = {"m": [], "r": []};
					CMData[CMType][CMChar] = {"i": CMIndex, "m": {"t": [""], "s": [], "u": [""]}, "r": {"t": [""], "s": [], "u": [""]}};
					CMPageIndex = {"m": 0, "r": 0};
					CMSortMap = getCMSortMap([], []);
					CMPageMap = getCMPageMap([], []);
					checkCMTableChanges(true);
					var checkPostReady = setInterval(function() {
						if (CMPostReady) {
							clearInterval(checkPostReady);
							postCM(0);
						}
					}, 200);
				} else {
					checkCMTableChanges(false);
					if (!CMInitData) {
						CMDataConvert();
					}
					CMSortMap = getCMSortMap(CMData[CMType][CMChar].m.s, CMData[CMType][CMChar].r.s);
					CMPageMap = getCMPageMap(CMData[CMType][CMChar].m.u, CMData[CMType][CMChar].r.u);
					CMSettingsCheck();
					if (CMVotes[CMType][CMChar] === undefined) {
						CMVotes[CMType][CMChar] = {"m": [], "r": []};
					}
				}
				if (CMData[CMType][CMChar] !== undefined) {
					CMPageIndex = { "m": $.inArray( CMPageMap.m[CMSettings[CMType][CMChar].m.p], CMSortMap.m ),
					                "r": $.inArray( CMPageMap.r[CMSettings[CMType][CMChar].r.p], CMSortMap.r ) };
				}
				loadCM(false, false);
			}
			$("#supplement-nav li:nth-child(2), #supplement-nav li:nth-child(3)").on("click", clickCMLessonTab);
			$("#supplement-nav li:last-child").on("click", clickLastLessonTab);
			$("#batch-items li:not(.active)").on("click", clickCMLessonItem);
		}
	}, 300);
}
/** @deprecated */
function clickCMLessonItem() {
	CMReady = false;
	setTimeout(function() {
		var prevType = CMType;
		CMChar = decodeURIComponent($("#character").html());
		CMType = ($("#main-info").attr("class") !== "radical") ? (($("#main-info").attr("class") === "kanji") ? "k" : "v") : "r";
		if (CMType !== "r") {
			if (CMType !== prevType) {
				$("#cm-meaning, #cm-reading").prev().remove();
				$("#cm-meaning, #cm-reading").remove();
				$('<h2>Community Meaning Mnemonic</h2><div id="cm-meaning" class="cm"><p class="loadingCM">Loading...</p></div>')
					.insertAfter($("#supplement-" + ((CMType === "k") ? "kan" : "voc") + "-meaning-notes"));
				$('<h2>Community Reading Mnemonic</h2><div id="cm-reading" class="cm"><p class="loadingCM">Loading...</p></div>')
					.insertAfter($("#supplement-" + ((CMType === "k") ? "kan" : "voc") + "-reading-notes"));
			} else {
				$("#cm-meaning, #cm-reading").html('<p class="loadingCM">Loading...</p>');
			}
			if (CMData[CMType][CMChar] === undefined) {
				CMIndex = Object.keys(CMData.k).length + Object.keys(CMData.v).length + 2;
				CMSettings[CMType][CMChar] = {"m": {"p": "", "c": false}, "r": {"p": "", "c": false}};
				CMVotes[CMType][CMChar] = {"m": [], "r": []};
				CMData[CMType][CMChar] = {"i": CMIndex, "m": {"t": [""], "s": [], "u": [""]}, "r": {"t": [""], "s": [], "u": [""]}};
				CMPageIndex = {"m": 0, "r": 0};
				CMSortMap = getCMSortMap([], []);
				CMPageMap = getCMPageMap([], []);
				checkCMTableChanges(true);
				var checkPostReady = setInterval(function() {
					if (CMPostReady) {
						clearInterval(checkPostReady);
						postCM(0);
					}
				}, 200);
			} else {
				checkCMTableChanges(false);
				if (!CMInitData) {
					CMDataConvert();
				}
				CMSortMap = getCMSortMap(CMData[CMType][CMChar].m.s, CMData[CMType][CMChar].r.s);
				CMPageMap = getCMPageMap(CMData[CMType][CMChar].m.u, CMData[CMType][CMChar].r.u);
				CMSettingsCheck();
				if (CMVotes[CMType][CMChar] === undefined) {
					CMVotes[CMType][CMChar] = {"m": [], "r": []};
				}
			}
			if (CMData[CMType][CMChar] !== undefined) {
				CMPageIndex = { "m": $.inArray( CMPageMap.m[CMSettings[CMType][CMChar].m.p], CMSortMap.m ),
				                "r": $.inArray( CMPageMap.r[CMSettings[CMType][CMChar].r.p], CMSortMap.r ) };
			}
			loadCM(false, false);
		}
		$("#supplement-nav li:last-child").off("click", clickLastLessonTab).on("click", clickLastLessonTab);
	}, 200);
}
/** @deprecated */
function clickCMLessonTab() {
	var failCount = 0;
	var checkReady = setInterval(function() {
		if (CMReady) {
			clearInterval(checkReady);
			var failCount = 0;
			if (Number($(this).attr("data-index")) === 1) {
				var checkMeaningVisible = setInterval(function() {
					if ($("#cm-meaning p").width() > 0) {
						clearInterval(checkMeaningVisible);
						updateCMMargins(true, false);
					} else if (failCount < 30) {
						failCount++;
					} else {
						clearInterval(checkMeaningVisible);
					}
				}, 100);
			} else {
				var checkReadingVisible = setInterval(function() {
					if ($("#cm-reading p").width() > 0) {
						clearInterval(checkReadingVisible);
						updateCMMargins(false, true);
					} else if (failCount < 30) {
						failCount++;
					} else {
						clearInterval(checkReadingVisible);
					}
				}, 100);
			}
		} else if (failCount < 10) {
			failCount++;
		}
		else {
			clearInterval(checkReady);
		}
	}, 200);
}
/** @deprecated */
function clickLastLessonTab() {
	if ($("#batch-items .read").length >= $("#batch-items li").length - 2) {
		var checkLessonQuiz = setInterval(function() {
			if ($("#quiz").css("display") !== "none") {
				clearInterval(checkLessonQuiz);
				initCMReview(true);
			}
		}, 500);
		$("#supplement-nav li").one("click", function() {
			clearInterval(checkLessonQuiz);
		});
	}
}

/**
 * @returns {ItemInfo | null} The character the current page is for, or null if it's a list
 */
function findCurrentCharacter() {
	var char, type;
	var regexMatches = /^\/(radicals|kanji|vocabulary)\/([^\/]+)$/.exec(window.location.pathname);
	if (regexMatches) {
		char = decodeURIComponent(regexMatches[2]);
		type = itemTypeFromString(regexMatches[1]);
	} else if ($("#character span").html()) {
		// Review pages
		char = $("#character span").html();
		type = itemTypeFromString($("#character").attr("class"));
	} else if ($("#main-info").attr("class")) {
		// Lesson pages
		char = $("#character").html();
		type = itemTypeFromString($("#main-info").attr("class"));
	} else {
		return null;
	}
	return findItem(type, char);
}

/**
 * Display the given info in the page
 * @param {Object.<string, ItemInfo>} [info=CMMainData.items]
 */
function displayInfo(info) {
	if (info === undefined) {
		info = CMMainData.items;
	}
	if (CMInfo.isList()) {
		initCMList();
	}
	var currentItem = findCurrentCharacter();
	if (currentItem) { CMInfo.item = currentItem; }
	$(".cm-meaning").html(getCMContentNew(CMInfo.item, MnemonicType.meaning));
	$(".cm-reading").html(getCMContentNew(CMInfo.item, MnemonicType.reading));
}

/**
 * Updates the current data with new data from a spreadsheet
 * @param {Array<SpreadsheetRow>} data
 * @param {Tabletop} tabletop
 */
function showInfo(data, tabletop) {
	CMMainData.lastUpdate = new Date();
	CMMainData.items = {};
	for (var i = 0; i < data.length; i++) {
		var newItem = parseSpreadsheetRow(data[i]);
		CMMainData.items[newItem.name()] = newItem;
	}
	sortMnemonicsByRating();
	displayInfo();
	saveVariables();
	return;
}
/** @deprecated */
function updateCMText(isMeaning) {
	var isMeaningStr = ((isMeaning) ? "m" : "r");
	$("#cm-" + ((isMeaning) ? "meaning" : "reading") + " .cm-mnem-text").html(CMData[CMType][CMChar][isMeaningStr].t[CMSortMap[isMeaningStr][CMPageIndex[isMeaningStr]]] +
		'<br />by <a href="https://www.wanikani.com/community/people/' + CMData[CMType][CMChar][isMeaningStr].u[CMSortMap[isMeaningStr][CMPageIndex[isMeaningStr]]] + '" target="_blank">' +
		CMData[CMType][CMChar][isMeaningStr].u[CMSortMap[isMeaningStr][CMPageIndex[isMeaningStr]]] + '</a>');
	checkCMTextHighlight(isMeaning);
	if (CMData[CMType][CMChar][isMeaningStr].u[CMSortMap[isMeaningStr][CMPageIndex[isMeaningStr]]] === CMUser) {
		$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-user-buttons div").removeClass("disabled");
	} else {
		$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-user-buttons div").addClass("disabled");
	}
	updateCMMargins(isMeaning, !isMeaning);
	$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-score-num").html(CMData[CMType][CMChar][isMeaningStr].s[CMSortMap[isMeaningStr][CMPageIndex[isMeaningStr]]]);
	if (CMData[CMType][CMChar][isMeaningStr].s[CMSortMap[isMeaningStr][CMPageIndex[isMeaningStr]]] !== 0) {
		if (CMData[CMType][CMChar][isMeaningStr].s[CMSortMap[isMeaningStr][CMPageIndex[isMeaningStr]]] > 0) {
			$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-score-num").removeClass("neg").addClass("pos");
		} else {
			$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-score-num").removeClass("pos").addClass("neg");
		}
	} else {
		$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-score-num").removeClass("pos").removeClass("neg");
	}
}
/** @deprecated */
function updateCMMargins(meaning, reading) {
	if (meaning) {
		$("#cm-meaning-info").css("margin-top", ($("#cm-meaning .cm-mnem-text").height() + 20 + ((CMIsReview || CMIsLesson) ? ((CMIsReview) ? 5 : 3) : 0)) + "px");
		$("#cm-meaning-user-buttons").css({"margin-top": "-20px"});
		$("#cm-meaning-next").css("margin-left", Math.min($("#cm-meaning .cm-mnem-text").width() + $("#cm-meaning-next").width() + 40,
		                                                  $("#cm-meaning").width() - $("#cm-meaning-next").width() - 10) + "px");
	}
	if (reading) {
		$("#cm-reading-info").css("margin-top", ($("#cm-reading .cm-mnem-text").height() + 20 + ((CMIsReview || CMIsLesson) ? ((CMIsReview) ? 5 : 3) : 0)) + "px");
		$("#cm-reading-user-buttons").css({"margin-top": "-20px"});
		$("#cm-reading-next").css("margin-left", Math.min($("#cm-reading .cm-mnem-text").width() + $("#cm-reading-next").width() + 40,
		                                                  $("#cm-reading").width() - $("#cm-reading-next").width() - 10) + "px");
	}
}
/** @deprecated */
function checkCMTextHighlight(isMeaning) {
	if (!CMIsReview && !CMIsLesson) {
		$("#cm-" + ((isMeaning) ? "meaning" : "reading") + " .cm-mnem-text").children("[class]").each(function() {
			if ($(this).attr("class").slice($(this).attr("class").indexOf("-") + 1) !== "highlight") {
				$(this).attr("class", $(this).attr("class").slice($(this).attr("class").indexOf("-") + 1) + "-highlight");
			}
		});
	}
}
/** @deprecated */
function checkCMHTMLTags(text) {
	var isValid = true;
	var tags = ["b", "i", "u", "s", "span"];
	var regOpen;
	var regClose;
	var regBR;
	if (!new RegExp("<(a|script)", "ig").test($("#cm-reading-text").val())) {
		for (var c = 0; c < tags.length; c++) {
			if (tags[c] !== "span") {
				regOpen = new RegExp("<" + tags[c] + ">", "ig");
				regClose = new RegExp("</" + tags[c] + ">", "ig");
			} else {
				regOpen = new RegExp("<" + tags[c], "ig");
				regClose = new RegExp("</" + tags[c], "ig");
			}
			if (text.match(regOpen) !== null && text.match(regClose) !== null) {
				if (text.match(regOpen).length !== text.match(regClose).length) {
					isValid = false;
				}
			} else if (text.match(regOpen) !== null || text.match(regClose) !== null) {
				isValid = false;
			}
			if (!isValid) { break; }
		}
	} else {
		return false;
	}
	return isValid;
}
/** @deprecated */
function checkCMTableChanges(post) {
	CMPostReady = false;
	try {
		Tabletop.init( { key: public_spreadsheet_url,
		                 callback: onTableLoaded,
		                 prettyColumnNames: false,
		                 simpleSheet: true } );
	} catch(e) {
		if (!CMIsReview) {
			$(".loadingCM").html("An error occurred while trying to access the database; reload the page to try again.");
		} else {
			alert('The following error occurred while trying to access the database: "' + e + '". If the problem persists, make sure your internet connection is working properly.');
		}
	}
	function onTableLoaded(data, tabletop) {
		CMTableData = data;
		initCMTableItems();
		CMIndex = getCMIndex() + 2;
		if (CMIndex < 2) {
			CMIndex += data.length + 1;
		}
		if (post) {
			CMPostReady = true;
		} else {
			if (!CMInitData && CMIndex < CMTableData.length) {
				if (!CMDataMatch(CMData[CMType][CMChar])) {
					CMData[CMType][CMChar].m.t = CMTableData[CMIndex].Meaning_Mnem.split("|");
					CMData[CMType][CMChar].m.u = CMTableData[CMIndex].Meaning_User.split("|");
					CMData[CMType][CMChar].r.t = CMTableData[CMIndex].Reading_Mnem.split("|");
					CMData[CMType][CMChar].r.u = CMTableData[CMIndex].Reading_User.split("|");
					CMVotes[CMType][CMChar] = getCMVotes();
				} else {
					CMDataConvert();
				}
			}
			CMReady = true;
		}
	}
}
/** @deprecated */
function initCMTableItems() {
	CMTableItems = [];
	for (var i = 0; i < CMTableData.length; i++) {
		CMTableItems[i] = CMTableData[i].Item;
	}
}
/** @deprecated */
function getCMIndex() {
	return $.inArray(CMType + CMChar, CMTableItems);
}
/** @deprecated */
function checkCMVotes(meaning, reading) {
	if (meaning && CMData[CMType][CMChar].m.t.length > 0 && CMVotes[CMType][CMChar].m[CMData[CMType][CMChar].m.u[CMSortMap.m[CMPageIndex.m]] + ":" + CMData[CMType][CMChar].m.t[CMSortMap.m[CMPageIndex.m]]] === undefined) {
		if (CMData[CMType][CMChar].m.t[0] !== "!") {
			CMVotes[CMType][CMChar].m[CMData[CMType][CMChar].m.u[CMSortMap.m[CMPageIndex.m]] + ":" + CMData[CMType][CMChar].m.t[CMSortMap.m[CMPageIndex.m]]] = 0;
			saveCMVotes();
		}
	}
	if (reading && CMData[CMType][CMChar].r.t.length > 0 && CMVotes[CMType][CMChar].r[CMData[CMType][CMChar].r.u[CMSortMap.r[CMPageIndex.r]] + ":" + CMData[CMType][CMChar].r.t[CMSortMap.r[CMPageIndex.r]]] === undefined) {
		if (CMData[CMType][CMChar].r.t[0] !== "!") {
			CMVotes[CMType][CMChar].r[CMData[CMType][CMChar].r.u[CMSortMap.r[CMPageIndex.r]] + ":" + CMData[CMType][CMChar].r.t[CMSortMap.r[CMPageIndex.r]]] = 0;
			saveCMVotes();
		}
	}
}
/** @deprecated */
function getCMVotes() {
	var votes = {"m": [], "r": []};
	for (var mv = 0; mv < CMData[CMType][CMChar].m.t.length; mv++) {
		if (votes.m[CMData[CMType][CMChar].m.u[mv] + ":" + CMData[CMType][CMChar].m.t[mv]] === undefined) {
			votes.m[CMData[CMType][CMChar].m.u[mv] + ":" + CMData[CMType][CMChar].m.t[mv]] = 0;
		}
	}
	for (var rv = 0; rv < CMData[CMType][CMChar].r.t.length; rv++) {
		if (votes.r[CMData[CMType][CMChar].r.u[rv] + ":" + CMData[CMType][CMChar].r.t[rv]] === undefined) {
			votes.r[CMData[CMType][CMChar].r.u[rv] + ":" + CMData[CMType][CMChar].r.t[rv]] = 0;
		}
	}
	return votes;
}
/** @deprecated */
function getCMSortMap(MScores, RScores) {
	var sortMap = {"m": [], "r": []};
	var MScoresSorted = [];
	var RScoresSorted = [];
	if (MScores[0] !== undefined && MScores[0] !== null) {
		MScoresSorted = MScores.slice(0).sort(sortByScore);
		for (var mu = 0; mu < MScores.length; mu++) {
			for (var ms = 0; ms < MScores.length; ms++) {
				if (sortMap.m[ms] === undefined && MScores[mu] === MScoresSorted[ms]) {
					sortMap.m[ms] = mu;
					break;
				}
			}
		}
	}
	if (RScores[0] !== undefined && RScores[0] !== null) {
		RScoresSorted = RScores.slice(0).sort(sortByScore);
		for (var ru = 0; ru < RScores.length; ru++) {
			for (var rs = 0; rs < RScores.length; rs++) {
				if (sortMap.r[rs] === undefined && RScores[ru] === RScoresSorted[rs]) {
					sortMap.r[rs] = ru;
					break;
				}
			}
		}
	}
	function sortByScore(a, b) {
		return b - a;
	}
	return sortMap;
}
/** @deprecated */
function getCMPageMap(MUsers, RUsers) {
	var pageMap = {"m": {"": 0}, "r": {"": 0}};
	for (var m = 0; m < MUsers.length; m++) {
		pageMap.m[MUsers[m]] = m;
	}
	for (var r = 0; r < RUsers.length; r++) {
		pageMap.r[RUsers[r]] = r;
	}
	return pageMap;
}

/**
 * Creates HTML for displaying the given item
 * @param {ItemInfo} item The item to create HTML for
 * @param {MnemonicType} mnemonicType The mnemonic type to display
 * @returns {Node[]}
 */
function getCMContentNew(item, mnemonicType) {
	var typeName = mnemonicTypeLongName(mnemonicType);
	var idprefix = "cm-" + typeName;
	var info = mnemonicType === MnemonicType.reading ? item.readingInfo() : item.meaningInfo();

	var newButton = createNode("div", {id: idprefix + "-submit", class: "cm-submit-highlight", child: "Submit Yours"});

	if (info.mnemonics.length > 0) {
		var selections = CMSaveData.selectedMnemonics[item.name()];
		selections = selections || {};
		var selectedAuthor = mnemonicType === MnemonicType.reading ? selections.reading : selections.meaning;
		var index = indexWhere(info.mnemonics, function(mnemonic) { return mnemonic.author === selectedAuthor; });
		if (index < 0) { index = 0; }
		var isAtBeginning = index <= 0;
		var isAtEnd = index >= info.mnemonics.length - 1;
		var mnemonic = info.mnemonics[index];
		var isAuthor = mnemonic.author === CMInfo.user;

		var prevButton = createNode("div", {id: idprefix + "-prev", classes: ["cm-prev", isAtBeginning ? "disabled" : ""], child: createNode("span", {child: "◄"})});
		var nextButton = createNode("div", {id: idprefix + "-next", classes: ["cm-next", isAtEnd       ? "disabled" : ""], child: createNode("span", {child: "►"})});
		var content = createNode("p", {class: "cm-mnem-text", children: mnemonic.createHTML()});

		var scores = createNode("div", {class: "cm-score", children: ["Score: ", createNode("span", {id: idprefix + "-score-num", classes: ["cm-score-num", mnemonic.votes !== 0 ? (mnemonic.votes > 0 ? "pos" : "neg") : ""], child: mnemonic.votes.toString()})]});
		var voteButtons = [createNode("div", {class: "cm-upvote-highlight", child: "Upvote"}), createNode("div", {class: "cm-downvote-highlight", child: "Downvote"})];
		var userButtons = createNode("div", {id: idprefix + "-user-buttons", class: "cm-user-buttons", children: [
			createNode("div", {classes: ["cm-edit-highlight",   isAuthor ? "" : "disabled"], child: "Edit"}),
			createNode("div", {classes: ["cm-delete-highlight", isAuthor ? "" : "disabled"], child: "Delete"})
		]});

		var infoField = createNode("div", {id: idprefix + "-info", class: "cm-info", children: [].concat(scores, voteButtons, userButtons, br(), newButton)});

		return [prevButton, content, nextButton, infoField];
	}
	else {
		var hasRequested = info.requests.indexOf(CMInfo.user) !== -1;
		var nocontent = createNode("p", {child: "Nobody has posted a mnemonic for this item's " + typeName + " yet. If you like, you can be the first to submit one!"});
		newButton.classList.add("nomnem");
		var requestButton = createNode("div", {id: idprefix + "-req", class: "cm-req-highlight", child: hasRequested ? "Delete Request" : "Make Request"});
		var requestList = [];
		if (info.requests.length > 0) {
			requestList = [br(), br(), createNode("p", {id: idprefix + "-reqtext", class: "cm-reqtext", children: [].concat("Mnemonic requested by: ", getCMReqList(info.requests)), other: {style: "margin-bottom: 0"}})];
		}
		return [nocontent, createNode("div", {id: idprefix + "-info", class: "cm-info", children: [].concat(newButton, requestButton, requestList)})];
	}
}
/** @deprecated */
function getCMContent(item, itemType, mnemType) {
	var CMItem = null;
	var CMMnemType = (mnemType === "m") ? "meaning" : "reading";
	var CMContent = '<p';
	var CMLen = CMData[itemType][item][mnemType].t.length;
	var CMPage = CMPageIndex[mnemType];
	if (CMData[itemType][item][mnemType].t[0].length > 0 && CMData[itemType][item][mnemType].t[0] !== "!") {
		CMItem = CMData[itemType][item][mnemType];
		CMContent = '<div id="cm-' + CMMnemType + '-prev" class="cm-prev'   + ((CMLen > 1 && CMPage > 0) ? "" : " disabled") + '"><span>◄</span></div>' + CMContent +  ' class="cm-mnem-text">' + CMItem.t[CMSortMap[mnemType][CMPage]] +
			'<br />by <a href="https://www.wanikani.com/community/people/' + CMItem.u[CMSortMap[mnemType][CMPage]] + '" target="_blank" >' +
			CMItem.u[CMSortMap[mnemType][CMPage]] + '</a></p><div id="cm-' + CMMnemType + '-next" class="cm-next' + ((CMLen > 1 && CMPage < CMLen - 1) ? "" : " disabled") + '"><span>►</span></div>' +
			'<div id="cm-' + CMMnemType + '-info" class="cm-info"><div class="cm-score">Score: <span id="cm-' + CMMnemType +
			'-score-num" class="cm-score-num' + ((CMItem.s[CMSortMap[mnemType][CMPage]] !== 0) ? ((CMItem.s[CMSortMap[mnemType][CMPage]] > 0) ? " pos" : " neg") : "") + '">' +
			CMItem.s[CMSortMap[mnemType][CMPage]] + '</span></div><div class="cm-upvote-highlight">Upvote</div><div class="cm-downvote-highlight">Downvote</div>' +
			'<div id="cm-' + CMMnemType + '-user-buttons" class="cm-user-buttons"><div class="cm-edit-highlight' + ((CMItem.u[CMSortMap[mnemType][CMPage]] !== CMUser) ? " disabled" : "") + '">Edit</div><div class="cm-delete-highlight' +
			((CMItem.u[CMSortMap[mnemType][CMPage]] !== CMUser) ? " disabled" : "") + '">Delete</div></div><br /><div id="cm-' + CMMnemType + '-submit" class="cm-submit-highlight">Submit Yours</div></div>';
	} else {
		CMContent += '>Nobody has posted a mnemonic for this item\'s ' + CMMnemType + ' yet. If you like, you can be the first to submit one!</p><div id="cm-' + CMMnemType + '-info" class="cm-info cm-nomnem">' +
		             '<div id="cm-' + CMMnemType + '-submit" class="cm-submit-highlight nomnem">Submit Yours</div><div id="cm-' + CMMnemType + '-req" class="cm-req-highlight">' +
		             (($.inArray(CMUser, CMData[itemType][item][mnemType].u) < 0) ? "Make Request" : "Delete Request") + '</div>';
		if (CMData[itemType][item][mnemType].t[0].length > 0 && CMData[itemType][item][mnemType].u[0].length > 0) {
			CMContent += '<br/><br/><p id="cm-' + CMMnemType + '-reqtext" class="cm-reqtext" style="margin-bottom: 0">Mnemonic requested by: ' + getCMReqList(CMData[itemType][item][mnemType].u) + "</p>";
		}
		CMContent += "</div>";
	}
	return CMContent;
}
/**
 * Format a list of users as a nice looking list in HTML
 * @param {string[]} users
 * @returns {(Node | string)[]}
 */
function getCMReqList(users) {
	var reqList = [];
	for (var u = 0; u < users.length; u++) {
		if (u > 0) {
			if (u < users.length - 1) {
				reqList.push(", ");
			} else {
				reqList.push(((u > 1) ? "," : "") + " and ");
			}
		}
		reqList.push(makeWKUserLink(users[u]));
	}
	return reqList;
}
/** @deprecated */
function getCMForm(mnemType) {
	var CMForm = '<form id="cm-' + mnemType + '-form" class="cm-form" onsubmit="return false"><div id="cm-' + mnemType + '-format" class="cm-format">' +
	             '<div class="btn cm-format-btn cm-format-bold"><b>b</b></div><div class="btn cm-format-btn cm-format-italic"><i>i</i></div><div class="btn cm-format-btn cm-format-underline"><u>u</u></div>' +
	             '<div class="btn cm-format-btn cm-format-strike"><s>s</s></div><div class="btn cm-format-btn cm-format-reading">読</div><div class="btn cm-format-btn cm-format-rad">部</div>' +
	             '<div class="btn cm-format-btn cm-format-kan">漢</div><div class="btn cm-format-btn cm-format-voc">語</div></div><fieldset><textarea id="cm-' +
	             mnemType + '-text" class="cm-text" maxlength="5000" placeholder="Submit a community mnemonic" style="overflow: hidden;' +
	             'word-wrap: break-word; resize: none; height: ' + ((CMIsReview) ? "53" : "55") + 'px;"></textarea><button id="cm-' + mnemType +'-form-cancel" class="cm-form-cancel">Cancel</button><button id="cm-' + mnemType + '-form-submit" class="cm-form-submit disabled" type="button">Submit</button><span class="counter-note"' +
	             'title="Characters Remaining">5000 <i class="icon-pencil"></i></span></fieldset></form>';
	return CMForm;
}
/** @deprecated */
function CMDataMatch(storedData) {
	return (CMType + CMChar === CMTableData[CMIndex - 2].Item && storedData.m.t === CMTableData[CMIndex - 2].Meaning_Mnem && storedData.r.t === CMTableData[CMIndex - 2].Reading_Mnem &&
	        storedData.m.u === CMTableData[CMIndex - 2].Meaning_User && storedData.r.u === CMTableData[CMIndex - 2].Reading_User);
}
/** @deprecated */
function CMDataConvert() {
	CMData[CMType][CMChar].m.t = $.map(CMData[CMType][CMChar].m.t, function(mt) { return mt; });
	CMData[CMType][CMChar].m.s = $.map(CMData[CMType][CMChar].m.s, function(ms) { return ms; });
	CMData[CMType][CMChar].m.u = $.map(CMData[CMType][CMChar].m.u, function(mu) { return mu; });
	CMData[CMType][CMChar].r.t = $.map(CMData[CMType][CMChar].r.t, function(rt) { return rt; });
	CMData[CMType][CMChar].r.s = $.map(CMData[CMType][CMChar].r.s, function(rs) { return rs; });
	CMData[CMType][CMChar].r.u = $.map(CMData[CMType][CMChar].r.u, function(ru) { return ru; });
}
/** @deprecated */
function CMSettingsCheck() {
	if (CMSettings[CMType][CMChar] === undefined) {
		CMSettings[CMType][CMChar] = {"m": {"p": CMData[CMType][CMChar].m.u[curSortMap.m[0]],
											"c": ($.inArray( CMUser, CMData[CMType][CMChar].m.u[curSortMap.m[0]] + 1))},
									  "r": {"p": CMData[CMType][CMChar].r.u[curSortMap.m[0]],
											"c": ($.inArray( CMUser, CMData[CMType][CMChar].r.u[curSortMap.r[0]] + 1))}};
	} else {
		if (($.inArray( CMSettings[CMType][CMChar].m.p, CMData[CMType][CMChar].m.u[CMSortMap.m[0]] )) < 0) {
			CMSettings[CMType][CMChar].m.p = CMData[CMType][CMChar].m.u[CMSortMap.m[0]];
		}
		if (($.inArray( CMSettings[CMType][CMChar].r.p, CMData[CMType][CMChar].r.u[CMSortMap.r[0]] )) < 0) {
			CMSettings[CMType][CMChar].r.p = CMData[CMType][CMChar].r.u[CMSortMap.r[0]];
		}
		if (($.inArray( CMUser, CMData[CMType][CMChar].m.u[CMSortMap.m[0]] ) > -1) !== CMSettings[CMType][CMChar].m.c) {
			CMSettings[CMType][CMChar].m.c = !CMSettings[CMType][CMChar].m.c;
		}
		if (($.inArray( CMUser, CMData[CMType][CMChar].r.u[CMSortMap.r[0]] ) > -1) !== CMSettings[CMType][CMChar].r.c) {
			CMSettings[CMType][CMChar].r.c = !CMSettings[CMType][CMChar].r.c;
		}
	}
}

function addCMStyles() {
	if (CMInfo.stylesAdded) { return; }
	CMInfo.stylesAdded = true;
	$("head").append('<style type="text/css">'
	                 // For lists
	               + '.legend.level-list ul { display: flex; flex-flow: row; }'
	               + '.legend.level-list li { flex: 1 1 auto; }'
	               + '.commnem-badge, .commnem-badge-req { position: absolute; left: 0 } '
	               + '.commnem-badge:before, .commnem-badge-req:before { '
	               + 'content: "\\5171"; display: block; position: absolute; top: -0.6em; left: -0.6em; width: 2em; height: 2em; color: #fff; font-size: 16px; font-weight: normal; '
	               + 'line-height: 2.2em; -webkit-box-shadow: 0 -2px 0 rgba(0,0,0,0.2) inset,0 0 10px rgba(255,255,255,0.5); '
	               + '-moz-box-shadow: 0 -2px 0 rgba(0,0,0,0.2) inset,0 0 10px rgba(255,255,255,0.5); box-shadow: 0 -2px 0 rgba(0,0,0,0.2) inset,0 0 10px rgba(255,255,255,0.5); '
	               + '-webkit-border-radius: 50%; -moz-border-radius: 50%; border-radius: 50%; z-index: 999 }'
	               + 'ul.multi-character-grid .commnem-badge:before, ul.multi-character-grid .commnem-badge-req:before { top: 1.1em; left: -1.1em; font-size: 11px; text-align: center }'
	               + '.commnem-badge:before { background-color: #71aa00; text-shadow: 0 2px 0 #1a5300; }'
	               + '.commnem-badge-req:before { background-color: #e1aa00; text-shadow: 0 2px 0 #7a5300 }'

	                 // For single-item displays
	               + '.cm-prev, .cm-next, .cm-upvote-highlight, .cm-downvote-highlight, .cm-delete-highlight, .cm-edit-highlight, .cm-submit-highlight, .cm-req-highlight { cursor: pointer !important }'
	               + '.cm-prev, .cm-next { font-size: 50px; margin: 0px 0px 0px 0px; padding: 15px 10px 0px 0px; float: left' + (CMInfo.isReviewOrLesson() ? "; margin-top: -10px; padding: 0 " : "" ) + '}'
	               + '.cm-prev.disabled, .cm-next.disabled { opacity: 0.25 }'
	               + '.cm-prev span, .cm-next span { background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgb(85, 85, 85)), to(rgb(70, 70, 70))); -webkit-background-clip: text; '
	               + '-webkit-text-fill-color: transparent; -webkit-text-stroke: 2px black; background: -moz-gradient(linear, 0% 0%, 0% 100%, from(rgb(85, 85, 85)), to(rgb(70, 70, 70)));'
	               + '-moz-background-clip: text; -moz-text-fill-color: transparent; -moz-text-stroke: 2px black }'
	               + '.cm-next { position: absolute; pointer-events: none; padding-left: 10px }'
	               + '.cm-next span { pointer-events: all }'
	               + '.cm-mnem-text { margin: 0 190px 0 60px' + (CMInfo.isReviewOrLesson() ? "; padding: 0" : "" ) + '}'
	               + '.cm-info { display: inline-block }'
	               + '.cm-info, .cm-info div { margin-bottom: 0px !important }'
	               + '.cm-score { float: left; width: 80px }'
	               + '.cm-score-num { color: #555 }'
	               + '.cm-score-num.pos { color: #5c5 }'
	               + '.cm-score-num.neg { color: #c55 }'
	               + '.cm-upvote-highlight, .cm-downvote-highlight, .cm-delete-highlight, .cm-edit-highlight, .cm-submit-highlight, .cm-req-highlight {'
	               + '    text-align: center; font-size: 14px; width: 75px; margin-right: 10px; float: left; background-repeat: repeat-x; cursor: help; padding: 1px 4px; color: #fff; '
	               + '    text-shadow: 0 1px 0 rgba(0,0,0,0.2); white-space: nowrap; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; '
	               + '    -webkit-box-shadow: 0 -2px 0 rgba(0,0,0,0.2) inset; -moz-box-shadow: 0 -2px 0 rgba(0,0,0,0.2) inset; box-shadow: 0 -2px 0 rgba(0,0,0,0.2) inset'
	               + '} .cm-upvote-highlight { background-image: linear-gradient(to bottom, #5c5, #46ad46) }'
	               + '.cm-downvote-highlight { background-image: linear-gradient(to bottom, #c55, #ad4646) }'
	               + '.cm-user-buttons { position: absolute; margin-top: -34px }'
	               + '.cm-delete-highlight, .cm-edit-highlight { font-size: 12px; width: 50px; height: 12px; line-height: 1 }'
	               + '.cm-delete-highlight { background-image: linear-gradient(to bottom, #811, #6d0606); margin-right: 0 }'
	               + '.cm-edit-highlight { background-image: linear-gradient(to bottom, #ccc, #adadad) }'
	               + '.cm-delete-highlight.disabled, .cm-edit-highlight.disabled { display: none; pointer-events: none }'
	               + '.cm-submit-highlight { margin-top: 10px; width: 100px; background-image: linear-gradient(to bottom, #555, #464646) }'
	               + '.cm-submit-highlight.disabled { color: #8b8b8b !important }'
	               + '.cm-req-highlight { margin-top: 10px; width: 100px; background-image: linear-gradient(to bottom, #ea5, #d69646) }'
	               + '.cm-nomnem { margin-top: -10px !important }' + (CMInfo.isReviewOrLesson() ? ".cm-form fieldset { clear: left }" : "")
	               + '.cm-format { margin: 0 !important }'
	               + '.cm-format-btn { background-color: #f5f5f5; '
	               + 'background-image: -moz-linear-gradient(top, #fff, #e6e6e6); background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#e6e6e6)); '
	               + 'background-image: -webkit-linear-gradient(top, #fff, #e6e6e6); background-image: -o-linear-gradient(top, #fff, #e6e6e6); '
	               + 'background-image: linear-gradient(to bottom, #fff, #e6e6e6); background-repeat: repeat-x; width: 10px; height: 10px; margin: 0 !important; '
	               + 'padding: ' + (CMInfo.isReviewOrLesson() ? "7px 13px 13px 7px" : "8px 12px 12px 8px") + '; line-height: 1; float: left }'
	               + '.cm-format-bold, .cm-format-underline, .cm-format-strike { padding-left: 10px; padding-right: 10px }'
	               + '.cm-text::selection { background-color: DeepSkyBlue; color: black; } '
	               + '.cm-format-kan, .cm-format-kan:hover { background-color: #f100a1; color: white; background-image: linear-gradient(to bottom, #f0a, #dd0093);} '
	               + '.cm-format-voc, .cm-format-voc:hover { ' + (CMInfo.isReviewOrLesson() ? "" : "float: none;") + ' background-color: #a100f1; color: white; background-image: linear-gradient(to bottom, #a0f, #9300dd); }'
	               + '.cm-format-reading, .cm-format-reading:hover { background-color: #474747; color: white; background-image: linear-gradient(to bottom, #555, #333); }'
	               + '.cm-format-rad, .cm-format-rad:hover { background-color: #00a1f1; color: white; background-image: linear-gradient(to bottom, #0af, #0093dd); }'
	               + '.cm-format-btn.active { background-color:#e6e6e6; background-color:#d9d9d9; background-image:none; outline:0; '
	               + '-webkit-box-shadow:inset 0 2px 4px rgba(0,0,0,0.15),0 1px 2px rgba(0,0,0,0.05);-moz-box-shadow:inset 0 2px 4px rgba(0,0,0,0.15),0 1px 2px rgba(0,0,0,0.05); '
	               + 'box-shadow:inset 0 2px 4px rgba(0,0,0,0.15),0 1px 2px rgba(0,0,0,0.05) }'
	               + '.cm-delete-text { position: absolute; opacity: 0; text-align: center }'
	               + '.cm-delete-text h3 { margin: 0 }</style>');
}

/**
 * @param {boolean} fromForm
 * @param {boolean} meaning
 */
function actualLoadCM(fromForm, meaning) {
	addCMStyles();
	$(".loadingCM").remove();
	$("#cm-meaning").html(getCMContentNew(CMInfo.item, MnemonicType.meaning));
	$("#cm-meaning-delete-text").css({ "width": $("#cm-meaning").width() + "px",
	                                   "height": $("#cm-meaning-delete-text h3").height() + "px",
	                                   "padding": (($("#cm-meaning").height() - $("#cm-meaning-delete-text h3").height())/2) + "px 0"});
	$("#cm-reading").html(getCMContentNew(CMInfo.item, MnemonicType.reading));
	$("#cm-reading-delete-text").css({ "width": $("#cm-reading").width() + "px",
	                                   "height": $("#cm-reading-delete-text h3").height() + "px",
	                                   "padding": (($("#cm-reading").height() - $("#cm-reading-delete-text h3").height())/2) + "px 0"});

}

/**
 * @deprecated
 * @param {boolean} fromForm
 * @param {boolean} meaning
 */
function loadCM(fromForm, meaning) {
	var failCount = 0;
	var checkCMReady = setInterval(function() {
		if (CMInfo.ready) {
			clearInterval(checkCMReady);
			CMInfo.preloadReady = true;
			if (!fromForm) {
				addCMStyles();
				$(".loadingCM").remove();
			} else {
				if (meaning) {
					$("#cm-meaning .note-meaning").remove();
				} else {
					$("#cm-reading .note-reading").remove();
				}
			}
			if (!fromForm) {
				checkCMVotes(true, true);
			}
			if (!fromForm || meaning) {
				$("#cm-meaning").html($(getCMContent(CMChar, CMType, "m")));
				$("#cm-meaning-delete-text").css({ "width": $("#cm-meaning").width() + "px",
				                                   "height": $("#cm-meaning-delete-text h3").height() + "px",
				                                   "padding": (($("#cm-meaning").height() - $("#cm-meaning-delete-text h3").height())/2) + "px 0"});
				updateCMMargins(true, false);
			}
			if (!fromForm || !meaning) {
				$("#cm-reading").html($(getCMContent(CMChar, CMType, "r")));
				$("#cm-reading-delete-text").css({ "width": $("#cm-reading").width() + "px",
				                                   "height": $("#cm-reading-delete-text h3").height() + "px",
				                                   "padding": (($("#cm-reading").height() - $("#cm-reading-delete-text h3").height())/2) + "px 0"});
				updateCMMargins(false, true);
			}
			checkCMTextHighlight(true);
			checkCMTextHighlight(false);
			$(".cm-prev span, .cm-next span").click(function() {
				if (!$(this).parent().hasClass("disabled")) {
					if ($(this).parent().parent().attr("id") === "cm-meaning") {
						if ($(this).parent().attr("id") === "cm-meaning-prev") {
							if (0 < CMPageIndex.m) {
								CMPageIndex.m--;
								CMSettings[CMType][CMChar].m.p = CMData[CMType][CMChar].m.u[CMSortMap.m[CMPageIndex.m]];
								$("#cm-meaning-next").removeClass("disabled");
								if (CMPageIndex.m < 1) {
									$("#cm-meaning-prev").addClass("disabled");
								}
								checkCMVotes(true, false);
							}
						} else {
							if (CMData[CMType][CMChar].m.t.length - 1 > CMPageIndex.m) {
								CMPageIndex.m++;
								CMSettings[CMType][CMChar].m.p = CMData[CMType][CMChar].m.u[CMSortMap.m[CMPageIndex.m]];
								$("#cm-meaning-prev").removeClass("disabled");
								if (CMPageIndex.m >= Object.keys(CMData[CMType][CMChar].m.t).length - 1) {
									$("#cm-meaning-next").addClass("disabled");
								}
								checkCMVotes(true, false);
							}
						}
						updateCMText(true);
					} else {
						if ($(this).parent().attr("id") === "cm-reading-prev") {
							if (0 < CMPageIndex.r) {
								CMPageIndex.r--;
								CMSettings[CMType][CMChar].r.p = CMData[CMType][CMChar].r.u[CMSortMap.r[CMPageIndex.r]];
								$("#cm-reading-next").removeClass("disabled");
								if (CMPageIndex.r < 1) {
									$("#cm-reading-prev").addClass("disabled");
								}
								checkCMVotes(false, true);
							}
						} else {
							if (CMData[CMType][CMChar].r.t.length - 1 > CMPageIndex.r) {
								CMPageIndex.r++;
								CMSettings[CMType][CMChar].r.p = CMData[CMType][CMChar].r.u[CMSortMap.r[CMPageIndex.r]];
								$("#cm-reading-prev").removeClass("disabled");
								if (CMPageIndex.r >= Object.keys(CMData[CMType][CMChar].r.t).length - 1) {
									$("#cm-reading-next").addClass("disabled");
								}
								checkCMVotes(false, true);
							}
						}
						updateCMText(false);
					}
					saveCMSettings();
				}
			});
			$(".cm-upvote-highlight, .cm-downvote-highlight").click(function() {
				var isMeaning = ($(this).parent().attr("id") === "cm-meaning-info");
				var key = CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u[CMSortMap[((isMeaning) ? "m" : "r")][CMPageIndex[((isMeaning) ? "m" : "r")]]] + ":" +
					CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t[CMSortMap[((isMeaning) ? "m" : "r")][CMPageIndex[((isMeaning) ? "m" : "r")]]];
				if ((Number(CMVotes[CMType][CMChar][((isMeaning) ? "m" : "r")][key]) === 0 || ($(this).hasClass("cm-upvote-highlight") &&
				     Number(CMVotes[CMType][CMChar][((isMeaning) ? "m" : "r")][key]) === -1) || ($(this).hasClass("cm-downvote-highlight") &&
				     Number(CMVotes[CMType][CMChar][((isMeaning) ? "m" : "r")][key]) === 1))) {
					if (CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u[CMSortMap[((isMeaning) ? "m" : "r")][CMPageIndex[((isMeaning) ? "m" : "r")]]] !== CMUser) {
						if (isMeaning) {
							if ($(this).hasClass("cm-upvote-highlight")) {
								CMVotes[CMType][CMChar].m[key]++;
							} else {
								CMVotes[CMType][CMChar].m[key]--;
							}
							if (CMVotes[CMType][CMChar].m[key] !== 0) {
								CMData[CMType][CMChar].m.s[CMSortMap.m[CMPageIndex.m]] += CMVotes[CMType][CMChar].m[key];
							} else {
								CMData[CMType][CMChar].m.s[CMSortMap.m[CMPageIndex.m]] += ($(this).hasClass("cm-upvote-highlight")) ? 1 : -1;
							}
							$("#cm-meaning-score-num").html(CMData[CMType][CMChar].m.s[CMSortMap.m[CMPageIndex.m]]);
						} else {
							if ($(this).hasClass("cm-upvote-highlight")) {
								CMVotes[CMType][CMChar].r[key]++;
							} else {
								CMVotes[CMType][CMChar].r[key]--;
							}
							if (CMVotes[CMType][CMChar].r[key] !== 0) {
								CMData[CMType][CMChar].r.s[CMSortMap.r[CMPageIndex.r]] += CMVotes[CMType][CMChar].r[key];
							} else {
								CMData[CMType][CMChar].r.s[CMSortMap.r[CMPageIndex.r]] += ($(this).hasClass("cm-upvote-highlight")) ? 1 : -1;
							}
							$("#cm-reading-score-num").html(CMData[CMType][CMChar].r.s[CMSortMap.r[CMPageIndex.r]]);
						}
						if (CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].s[CMSortMap[((isMeaning) ? "m" : "r")][CMPageIndex[((isMeaning) ? "m" : "r")]]] !== 0) {
							if (CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].s[CMSortMap[((isMeaning) ? "m" : "r")][CMPageIndex[((isMeaning) ? "m" : "r")]]] > 0) {
								$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-score-num").removeClass("neg").addClass("pos");
							} else {
								$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-score-num").removeClass("pos").addClass("neg");
							}
						} else {
							$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-score-num").removeClass("pos").removeClass("neg");
						}
						if (Number(CMVotes[CMType][CMChar][((isMeaning) ? "m" : "r")][key]) === -1) {
							if ((isMeaning && parseInt(CMTableData[CMIndex - 2].Meaning_Score.split("|")[CMSortMap.m[CMPageIndex.m]]) <= -9) ||
							    (!isMeaning && parseInt(CMTableData[CMIndex - 2].Reading_Score.split("|")[CMSortMap.r[CMPageIndex.r]]) <= -9)) {
								deleteCM(isMeaning, false);
							} else {
								postCM(1);
							}
						} else {
							postCM(1);
						}
					} else {
						alert("Sorry, you can't vote on your own mnemonic.");
					}
				} else {
					alert("It looks like you've already " + ($(this).hasClass("cm-upvote-highlight") ? "up" : "down") + "voted this mnemonic.");
				}
			});
			$(".cm-delete-highlight").click(function() {
				var isMeaning = ($(this).parent().attr("id") === "cm-meaning-user-buttons");
				if ($(this).html() === "Delete") {
					$(this).html("Really Delete?").css({"font-size": "8px", "line-height": ((CMIsReview || CMIsLesson) ? "1.5" : "1.8")});
					setTimeout(function() {
						if ($("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-info .cm-delete-highlight").attr("disabled") === undefined) {
						   $("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-info .cm-delete-highlight").html("Delete").css({"font-size": "12px", "line-height": "1"});
						}
					}, 3000);
				} else {
					$(this).attr("disabled", "disabled");
					deleteCM(isMeaning, true);
				}
			});
			$(".cm-edit-highlight, .cm-submit-highlight").click(function() {
				var isEdit = ($(this).hasClass("cm-edit-highlight"));
				$(this).attr("disabled", "disabled");
				var isMeaning = ((isEdit && $(this).parent().attr("id") === "cm-meaning-user-buttons") || $(this).attr("id") === "cm-meaning-submit");
				if ((isEdit && ((isMeaning && CMSettings[CMType][CMChar].m.c) || (!isMeaning && CMSettings[CMType][CMChar].r.c))) ||
					(!isEdit && ((isMeaning && !CMSettings[CMType][CMChar].m.c) || (!isMeaning && !CMSettings[CMType][CMChar].r.c)))) {
					if (isEdit || CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t.length < 10) {
						if (isMeaning) {
							$("#cm-meaning").html('<div class="note-meaning noswipe">' + getCMForm("meaning") + '</div>');
						} else {
							$("#cm-reading").html('<div class="note-reading noswipe">' + getCMForm("reading") + '</div>');
						}
						if (isEdit) {
							$("#cm-" + ((isMeaning) ? "meaning" : "reading") + " .cm-text").val(CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t[CMSortMap[((isMeaning) ? "m" : "r")][CMPageIndex[((isMeaning) ? "m" : "r")]]])
								.css({"transition-duration": "0s", "-webkit-transition-duration": "0s", "-moz-transition-duration": "0s", "-o-transition-duration": "0s"});
							$("#cm-" + ((isMeaning) ? "meaning" : "reading") + " .cm-text").autosize({"callback": function() {
									setTimeout(function() {
										$("#cm-" + ((isMeaning) ? "meaning" : "reading") + " .cm-text").css({"transition-duration": "0.2s", "-webkit-transition-duration": "0.2s", "-moz-transition-duration": "0.2s", "-o-transition-duration": "0.2s"});
									}, 200);
								}
							});
							$("#cm-" + (($(this).attr("id") === "cm-meaning-text") ? "meaning" : "reading") + "-form .counter-note").html($("#cm-" + ((isMeaning) ? "meaning" : "reading") +
								" .cm-text").attr("maxlength")-$("#cm-" + ((isMeaning) ? "meaning" : "reading") + " .cm-text").val().length + ' <i class="icon-pencil"></i>');
						}
						$("#cm-" + ((isMeaning) ? "meaning" : "reading") + " .cm-text").keydown(function() {
							if (CMIsReview && !CMIsLesson) {
								if ( event.which === 8 && (!CMIsChrome || $("#deleteButton").length > 0) ) {
									var selStart = $("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-text").prop("selectionStart");
									var selEnd = $("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-text").prop("selectionEnd");
									if (selEnd === CMSelTemp && selStart === selEnd) {
										selStart -= (selEnd - $(this).val().lastIndexOf("<", selStart));
										if ($(".cm-format-btn.active").length > 0) {
											$(".cm-format-btn").removeClass("active");
											CMSelTemp = -1;
										} else if ($(this).val().substr(selStart - 1, 1) === ">") {
											switch($(this).val().slice($(this).val().lastIndexOf("<", selStart - 1) + 1, selStart - 1)) {
												case "b":
													$(".cm-format-bold").addClass("active");
													break;
												case "i":
													$(".cm-format-italic").addClass("active");
													break;
												case "u":
													$(".cm-format-underline").addClass("active");
													break;
												case "s":
													$(".cm-format-strike").addClass("active");
													break;
												case 'span class="highlight-reading"':
												case 'span class="reading-highlight"':
													$(".cm-format-reading").addClass("active");
													break;
												case 'span class="highlight-radical"':
												case 'span class="radical-highlight"':
													$(".cm-format-rad").addClass("active");
													break;
												case 'span class="highlight-kanji"':
												case 'span class="kanji-highlight"':
													$(".cm-format-kan").addClass("active");
													break;
												case 'span class="highlight-vocabulary"':
												case 'span class="vocabulary-highlight"':
													$(".cm-format-voc").addClass("active");
													break;
												default:
											}
											CMSelTemp = selStart;
										}
									}
									if (selStart === selEnd) {
										if (selStart > 0) {
											$(this).val($(this).val().slice(0, selEnd - 1) + $(this).val().slice(selEnd)).selectRange(selEnd - 1);
										}
									} else {
										$(this).val($(this).val().slice(0, selStart) + $(this).val().slice(selEnd)).selectRange(selStart);
									}
									$("#cm-" + (($(this).attr("id") === "cm-meaning-text") ? "meaning" : "reading") + "-form .counter-note").html($(this).attr("maxlength")-$(this).val().length + '<i class="icon-pencil"></i>');
									$(this).trigger("input");
								}
							}
							if (event.which === 190) {
								$(".cm-format-btn").removeClass("active");
							}
						});
						$("#cm-" + ((isMeaning) ? "meaning" : "reading") + " .cm-text").on('input propertychange', function(e) {
							if ($(this).val() !== "") {
								$(".cm-form-submit").removeClass("disabled");
							} else {
								$("button[type=submit]").addClass("disabled");
							}
							$(this).autosize();
							$("#cm-" + (($(this).attr("id") === "cm-meaning-text") ? "meaning" : "reading") + "-form .counter-note").html($(this).attr("maxlength")-$(this).val().length + '<i class="icon-pencil"></i>');
						}).focus();
						$(".cm-format-btn").click(function() {
							var isMeaningStr = ($(this).parent().attr("id") === "cm-meaning-format") ? "meaning" : "reading";
							var selStart = $("#cm-" + isMeaningStr + "-text").prop("selectionStart");
							var selEnd = $("#cm-" + isMeaningStr + "-text").prop("selectionEnd");
							var formatType = "";
							var tagStart = "";
							var tagEnd = "";
							var includesClass = false;
							CMSelTemp = -1;
							switch($(this).attr("class").slice($(this).attr("class").lastIndexOf("-") + 1, ((!$(this).hasClass("active")) ? $(this).attr("class").length : $(this).attr("class").lastIndexOf(" ")))) {
								case "bold":
									tagStart = "b";
									break;
								case "italic":
									tagStart = "i";
									break;
								case "underline":
									tagStart = "u";
									break;
								case "strike":
									tagStart = "s";
									break;
								case "reading":
									tagStart = 'span class="highlight-reading"';
									includesClass = true;
									break;
								case "rad":
									tagStart = 'span class="highlight-radical"';
									includesClass = true;
									break;
								case "kan":
									tagStart = 'span class="highlight-kanji"';
									includesClass = true;
									break;
								case "voc":
									tagStart = 'span class="highlight-vocabulary"';
									includesClass = true;
									break;
								default:
									tagStart = "span";
							}
							if (!$(this).hasClass("active")) {
								if ($(this).parent().children(".active").length > 0) {
									tagEnd = "</" + ((CMLastTag.indexOf(" ") < 0) ? CMLastTag : CMLastTag.slice(0, CMLastTag.indexOf(" "))) + ">";
									$("#cm-" + isMeaningStr + "-text").val($("#cm-" + isMeaningStr + "-text").val().slice(0, selStart) + tagEnd + $("#cm-" + isMeaningStr + "-text").val().slice(selStart));
									if (selStart === selEnd) {
										selStart += tagEnd.length;
									}
									selEnd += tagEnd.length;
								}
								$(".cm-format-btn").removeClass("active");
								CMSelTemp = selEnd + tagStart.length + 2;
								if (selStart === selEnd) {
									$("#cm-" + isMeaningStr + "-text").val($("#cm-" + isMeaningStr + "-text").val().slice(0, selStart) + "<" + tagStart +  ">" + $("#cm-" + isMeaningStr + "-text").val().slice(selStart));
									$(this).addClass("active");
								} else {
									$("#cm-" + isMeaningStr + "-text").val($("#cm-" + isMeaningStr + "-text").val().slice(0, selStart) + "<" + tagStart + ">" +
										$("#cm-" + isMeaningStr + "-text").val().slice(selStart, selEnd) + "</" + ((!includesClass) ? tagStart : tagStart.slice(0, tagStart.indexOf(" "))) + ">"　+
										$("#cm-" + isMeaningStr + "-text").val().slice(selEnd));
								}
							} else {
								if (selStart !== selEnd) {
									$("#cm-" + isMeaningStr + "-text").val($("#cm-" + isMeaningStr + "-text").val().slice(0, selStart) + $("#cm-" + isMeaningStr + "-text").val().slice(selEnd));
									selEnd = selStart;
								}
								tagEnd = "</" + ((!includesClass) ? tagStart : tagStart.slice(0, tagStart.indexOf(" "))) + ">";
								CMSelTemp = selStart + tagEnd.length;
								$("#cm-" + isMeaningStr + "-text").val($("#cm-" + isMeaningStr + "-text").val().slice(0, selStart) + tagEnd + $("#cm-" + isMeaningStr + "-text").val().slice(selStart))
								                                  .selectRange(CMSelTemp);
								$(this).removeClass("active");
							}
							$("#cm-" + isMeaningStr + "-text").trigger("propertychange").focus();
							CMLastTag = tagStart;
						});
						$(".cm-form-submit").click(function() {
							$(this).prop("disabled", true).prev().prop("disabled", true).parent().prop("disabled", true);
							$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-format .active").trigger("click");
							var mnemText = "";
							if (isEdit || !CMSettings[CMType][CMChar][((isMeaning) ? "m" : "r")].c) {
								mnemText = $("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-text").val().trim();
								if (mnemText.length > 0) {
									if (!/[|]/.test(mnemText)) {
										if (!new RegExp(Base64.decode("ZnVja3xzaGl0fGRpY2t8bXkgY29ja3x5b3VyIGNvY2t8cyBjb2NrfHBlbmlzfHJhcGV8cmFwaW5nfHB1c3N5fGN1bnR8Y2xpdHxqaXp6fGN1bXxhbnVzfG5pZ2dlcnxmYWd8d" +
											                          "Gl0fHBvcm58bW9sZXN0fHBlZG98aGF2ZSBzZXh8aGFkIHNleA==").toLowerCase(), "i").test(mnemText)) {
											if (checkCMHTMLTags(mnemText)) {
												var firstChar = (mnemText.substring(0, 1) !== "<") ? mnemText.substring(0, 1) : mnemText.substring(mnemText.indexOf(">") + 1, 1);
												if (firstChar !== "!" ) {
													mnemText = (mnemText.substr(0, 1) !== "<") ? (firstChar.toUpperCase() + mnemText.substring(1)) : (mnemText.slice(0, mnemText.indexOf(">") + 1) +
													            mnemText.substr(mnemText.indexOf(">") + 1, 1).toUpperCase() + mnemText.substring(mnemText.indexOf(">") + 2));
													if (!/[.|!|?|。|！]$/.test(mnemText.slice(mnemText.length - 1)) && mnemText.length < 500) {
														if (mnemText.slice(mnemText.length - 1) === ">") {
															mnemText += (!/[.|!|?|。|！]$/.test(mnemText.substr(mnemText.lastIndexOf("<") - 1, 1))) ? "." : "";
														}
														else {
															mnemText += ".";
														}
													}
													if (isEdit) {
														var index = CMSortMap[((isMeaning) ? "m" : "r")][CMPageIndex[((isMeaning) ? "m" : "r")]];
														if (CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t[index] !== (mnemText)) {
															CMVotes[CMType][CMChar][((isMeaning) ? "m" : "r")][CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u[index] + ":" + mnemText] = CMVotes[CMType][CMChar][((isMeaning) ? "m" : "r")][CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u[index] + ":" + CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t[index]];
															delete CMVotes[CMType][CMChar][((isMeaning) ? "m" : "r")][CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u[index] + ":" + CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t[index]];
															CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t[index] = mnemText;
															postCM(((isMeaning) ? 6 : 7));
														} else {
															$(this).prev().prop("disabled", false).trigger("click").prop("disabled", true);
														}
													} else {
														if (CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t[0].length > 0 && CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t[0] !== "!") {
															CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t.push(mnemText);
															CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].s.push(0);
															CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u.push(CMUser);
														} else {
															CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t = [mnemText];
															CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].s = [0];
															CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u = [CMUser];
														}
														CMVotes[CMType][CMChar][((isMeaning) ? "m" : "r")][CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u[CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u.length - 1] +
															":" + CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t[CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t.length - 1]] = 0;
														if (CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].s.length < 1) {
															CMSortMap[((isMeaning) ? "m" : "r")].push(0);
															CMPageIndex[((isMeaning) ? "m" : "r")] = 0;
														} else {
															for (var s = CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].s.length - 1; s >= 0; s--) {
																if (CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].s[s] > -1 || s === 0) {
																	CMSortMap[((isMeaning) ? "m" : "r")] = CMSortMap[((isMeaning) ? "m" : "r")].slice(0, s).concat([CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].s.length - 1].concat(CMSortMap[((isMeaning) ? "m" : "r")].slice(s)));
																	CMPageIndex[((isMeaning) ? "m" : "r")] = s;
																	break;
																}
															}
														}
														CMPageMap[((isMeaning) ? "m" : "r")][CMUser] = CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t.length - 1;
														postCM(((isMeaning) ? 2 : 3));
													}
												} else {
													alert("You may not begin a mnemonic with an exclamation mark (!) because it is used to indicate mnemonic requests.");
													$(this).prop("disabled", false).prev().prop("disabled", false).parent().prop("disabled", false);
												}
											} else {
												alert("Your mnemonic contains unclosed, unopened, or restricted HTML tags. These must be fixed before you can post your mnemonic.");
												$(this).prop("disabled", false).prev().prop("disabled", false).parent().prop("disabled", false);
											}
										} else {
											alert("Your mnemonic contains inappropriate or otherwise vulgar content. Please change your mnemonic to be more family-friendly because young children may use this script too.");
											$(this).prop("disabled", false).prev().prop("disabled", false).parent().prop("disabled", false);
										}
									} else {
										alert("You may not use the vertical bar (|) in a mnemonic because it is used in the database as a separator.");
										$(this).prop("disabled", false).prev().prop("disabled", false).parent().prop("disabled", false);
									}
								} else {
									alert("Sorry, that input is invalid. Please enter a proper mnemonic.");
									$(this).prop("disabled", false).prev().prop("disabled", false).parent().prop("disabled", false);
								}
							} else {
								alert("Sorry, you've already submitted a " + ((isMeaning) ? "meaning" : "reading") + " mnemonic for this item. You must delete your old " + ((isMeaning) ? "meaning" : "reading") + " mnemonic submission to submit a new one.");
								$(this).prop("disabled", false).prev().prop("disabled", false).parent().prop("disabled", false);
							}
						});
						$(".cm-form-cancel").click(function() {
							$(this).prop("disabled", true).next().attr("disabled", "disabled").parent().prop("disabled", true);
							if ($(this).attr("id") === "cm-meaning-form-cancel") {
								loadCM(true, true);
							}
							else {
								loadCM(true, false);
							}
						});
					} else {
						alert("Sorry, but it appears that the limit of 10 mnemonics for this item's " + ((isMeaning) ? "meaning" : "reading") + " has been reached.");
					}
				} else {
					alert("Sorry, you've already submitted a " + ((isMeaning) ? "meaning" : "reading") + " mnemonic for this item. You must delete your old " +
					      ((isMeaning) ? "meaning" : "reading") + " mnemonic submission to submit a new one.");
				}
			});
			$(".cm-req-highlight").click(function() {
				var isMeaning = ($(this).attr("id") === "cm-meaning-req");
				var hasRequested = $.inArray(CMUser, CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u) > -1;
				var reqText = $(this).next().next().next().html();
				if (!hasRequested) {
					if (CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u.length < 10) {
						if (CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u[0].length > 0) {
							CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u.push(CMUser);
							$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-reqtext").html("Mnemonic requested by: " + getCMReqList(CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u));
						} else {
							CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t = ["!"];
							CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u = [CMUser];
							$('<p id="cm-' + ((isMeaning) ? "meaning" : "reading") + '-reqtext" class="cm-reqtext" style="margin-bottom: 0">Mnemonic requested by: ' +
							  getCMReqList(CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u)).insertAfter($(this));
						}
						$(this).text("Delete Request");
						saveCMData();
						postCM(8);
					} else {
						alert("Sorry, but it appears that the limit of 10 mnemonic requests for this item's " + ((isMeaning) ? "meaning" : "reading") + " has been reached.");
					}
				} else {
					if (CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u.length > 1) {
						CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u.splice(CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u.indexOf(CMUser), 1);
						$(this).next().next().next().html("Mnemonic requested by: " + getCMReqList(CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u));
					} else {
						CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].t = [""];
						CMData[CMType][CMChar][((isMeaning) ? "m" : "r")].u = [""];
						$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-reqtext").remove();
						$(this).parent().children("br").remove();
					}
					$(this).text("Make Request");
					saveCMData();
					postCM(9);
				}
			});
		} else {
			failCount++;
			if (failCount === 10) {
				$(".loadingCM").html('Loading seems to be taking longer than it should and something may have gone wrong. If this issue continues after reloading the page, make sure your ' +
				                     'internet connection is working properly. If your internet is working properly and the issue persists, try temporarily disabling all other ' +
				                     'userscripts and extensions and try again. If it persists and you\'ve ensured that other userscripts or extensions are not the cause, check ' +
				                     '<a href="https://www.wanikani.com/chat/api-and-third-party-apps/7568" target="_blank">here</a> to see if anyone else is having this issue and, ' +
				                     'if not, please report it and include your operating system, browser, and version of the WaniKani Community Mnemonics userscript ' +
				                     '(which should be the newest to ensure minimal bugs).');
				clearInterval(checkCMReady);
			} else if (failCount < 10) {
				$(".loadingCM").html($(".loadingCM").html() + ".");
			}
		}
	}, 1000);
}
/** @deprecated */
function deleteCM(isMeaning, isUser) {
	var meaningReading = isMeaning ? "meaning" : "reading";
	var existingHTML = $("#cm-" + meaningReading);
	var newHTML = $('<div id="cm-' + meaningReading + '-delete-text" class="cm-delete-text"><h3>Deleting Mnemonic...</h3></div>');
	newHTML.insertBefore(existingHTML);
	newHTML.css({
		"width": existingHTML.width() + "px",
		"height": existingHTML.prev().children("h3").height() + "px",
		"padding": ((existingHTML.height() - newHTML.children("h3").height())/2) + "px 0"
	});
	existingHTML.add(newHTML).css({
		"-webkit-transition": "opacity 1s ease-in-out",
		"-moz-transition": "opacity 1s ease-in-out",
		"-o-transition": "opacity 1s ease-in-out",
		"transition": "opacity 1s ease-in-out"
	});
	existingHTML.css("opacity", "0");
	newHTML.css("opacity", "1");
	var curItem = CMData[CMType][CMChar][((isMeaning) ? "m" : "r")];
	var curItemSettings = CMSettings[CMType][CMChar][((isMeaning) ? "m" : "r")];
	var curItemVotes = CMVotes[CMType][CMChar][((isMeaning) ? "m" : "r")];
	var delIndex = CMSortMap[((isMeaning) ? "m" : "r")][CMPageIndex[((isMeaning) ? "m" : "r")]];
	var delUser = curItemSettings.p;
	delete CMVotes[CMType][CMChar][((isMeaning) ? "m" : "r")][curItem.u[delIndex] + ":" + curItem.t[delIndex]];
	if (curItem.t.length > 0) {
		curItem.t.splice(delIndex, 1);
	} else {
		curItem.t[0] = "";
	}
	curItem.s.splice(delIndex, 1);
	if (curItem.u.length > 0) {
		curItem.u.splice(delIndex, 1);
	} else {
		curItem.u[0] = "";
	}
	CMSettings[CMType][CMChar][((isMeaning) ? "m" : "r")] = {"p": ((delIndex > 0) ? curItem.u[CMSortMap[((isMeaning) ? "m" : "r")][CMPageIndex[((isMeaning) ? "m" : "r")] - 1]] : curItemSettings.p), "c": false};
	CMSortMap[((isMeaning) ? "m" : "r")] = getCMSortMap(((isMeaning) ? curItem.s : []), ((!isMeaning) ? curItem.s : []))[((isMeaning) ? "m" : "r")];
	CMPageMap[((isMeaning) ? "m" : "r")] = getCMPageMap(((isMeaning) ? curItem.u : []), ((!isMeaning) ? curItem.u : []))[((isMeaning) ? "m" : "r")];
	if (CMPageIndex[((isMeaning) ? "m" : "r")] >= curItem.t.length) {
		CMPageIndex[((isMeaning) ? "m" : "r")]--;
		curItemSettings.p = curItem.u[CMSortMap[((isMeaning) ? "m" : "r")][CMPageIndex[((isMeaning) ? "m" : "r")]]];
		$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-next").addClass("disabled");
		if (delIndex <= 1) {
			$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-prev").addClass("disabled");
		}
	} else if (delIndex < 1) {
		$("#cm-" + ((isMeaning) ? "meaning" : "reading") + "-prev").addClass("disabled");
	}
	postCM(((isMeaning) ? 4 : 5));
}
/** @deprecated */
function postCM(postType) {
	if (postType === 0) {
		CMData[CMType][CMChar].i = CMIndex;
	}
	var serializedData = (postType === 0) ? 'Item=' + CMType + encodeURIComponent(CMChar) + "&Meaning_Mnem=&Reading_Mnem=&Meaning_Score=&Reading_Score=&Meaning_User=&Reading_User=&Index=" +
		CMData[CMType][CMChar].i : "Item=" + CMType + encodeURIComponent(CMChar) + "&Meaning_Mnem=" + encodeURIComponent(CMData[CMType][CMChar].m.t.join("|")) + "&Reading_Mnem=" +
		encodeURIComponent(CMData[CMType][CMChar].r.t.join("|")) + "&Meaning_Score=" + ((!isNaN(CMData[CMType][CMChar].m.s[0])) ? CMData[CMType][CMChar].m.s.join("|") : "") + "&Reading_Score=" +
		((!isNaN(CMData[CMType][CMChar].r.s[0])) ? CMData[CMType][CMChar].r.s.join("|") : "") + "&Meaning_User=" + CMData[CMType][CMChar].m.u.join("|") + "&Reading_User=" +
		CMData[CMType][CMChar].r.u.join("|") + "&Index=" + CMData[CMType][CMChar].i;
	if ((postType !== 2 && postType !== 3) || CMSettings[CMType][CMChar][((postType === 2) ? "m" : "r")].c === false) {
		if (postType === 2 || postType === 3) {
			CMSettings[CMType][CMChar][((postType === 2) ? "m" : "r")].c = true;
		}
		$.ajax({
			url: "https://script.google.com/macros/s/AKfycbznhpL43Ix-qqO3sNcJmQeQk5dsdW6u0uaZ9to4_8TQho0qcm0/exec",
			type: "POST",
			data: serializedData,
			success: function(data, textStatus, jqXHR) {
				if (postType === 0) {
					CMReady = true;
				} else {
					if (postType > 1) {
						if (postType === 2 || postType === 3) {
							CMSettings[CMType][CMChar][((postType === 2) ? "m" : "r")].p = CMUser;
							saveCMSettings();
							loadCM(true, (postType === 2));
						} else if (postType < 6) {
							$("#cm-" + [((postType === 4) ? "meaning" : "reading")] + "-delete-text h3").html("Mnemonic successfully deleted!");
							if ((postType === 4 && CMData[CMType][CMChar].m.t.length > 0) || (postType === 5 && CMData[CMType][CMChar].r.t.length > 0)) {
								updateCMText((postType === 4));
							} else {
								CMData[CMType][CMChar][((postType === 4) ? "m" : "r")] = {"t": [""], "s": [], "u": [""]};
								loadCM(true, (postType === 4));
							}
							saveCMSettings();
							setTimeout(function() {
								$("#cm-" + ((postType === 4) ? "meaning" : "reading") + "-user-buttons .cm-delete-highlight").html("Delete").css({"font-size": "12px", "line-height": "1"}).removeAttr("disabled");
								$("#cm-" + ((postType === 4) ? "meaning" : "reading")).css("opacity", "1").prev().css("opacity", "0");
								setTimeout(function() {
									$("#cm-" + ((postType === 4) ? "meaning" : "reading")).css({ "-webkit-transition": "none",
									                                                             "-moz-transition": "none",
									                                                             "-o-transition": "none",
									                                                             "transition": "none"}).prev().remove();
								}, 1000);
							}, 3000);
						} else if (postType < 8) {
							updateCMText((postType === 6));
							loadCM(true, (postType === 6));
						}
					}
					saveCMVotes();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert("Error submitting data to database.");
			}
		});
	}
}
/** @deprecated */
function saveCMData() {
	   localStorage.setItem("CMData", JSON.stringify(CMData, function (key, value) {
		//Start Code Credit: Mike Samuel from Stack Overflow
		if (value && typeof value === 'object') {
			var replacement = {};
			for (var k in value) {
				if (Object.hasOwnProperty.call(value, k)) {
					replacement[decodeURIComponent(k)] = value[k];
				}
			}
			return replacement;
		}
		return value;
		//End Code Credit
	}));
}
/** @deprecated */
function saveCMSettings() {
	localStorage.setItem("CMSettings", JSON.stringify(CMSettings, function (key, value) {
		//Start Code Credit: Mike Samuel from Stack Overflow
		if (value && typeof value === 'object') {
			var replacement = {};
			for (var k in value) {
				if (Object.hasOwnProperty.call(value, k)) {
					replacement[decodeURIComponent(k)] = value[k];
				}
			}
			return replacement;
		}
		return value;
		//End Code Credit
	}));
}
/** @deprecated */
function saveCMVotes() {
	localStorage.setItem("CMVotes", JSON.stringify(CMVotes, function (key, value) {
		//Start Code Credit: Mike Samuel from Stack Overflow
		if (value && typeof value === 'object') {
			var replacement = {};
			for (var k in value) {
				if (Object.hasOwnProperty.call(value, k)) {
					replacement[k] = value[k];
				}
			}
			return replacement;
		}
		return value;
		//End Code Credit
	}));
}
/** @deprecated */
//Start Code Credit: Mark from Stack Overflow
$.fn.selectRange = function(start, end) {
	if(!end) {
		end = start;
	}
	return this.each(function() {
		if (this.setSelectionRange) {
			this.focus();
			this.setSelectionRange(start, end);
		} else if (this.createTextRange) {
			var range = this.createTextRange();
			range.collapse(true);
			range.moveEnd('character', end);
			range.moveStart('character', start);
			range.select();
		}
	});
};
//End Code Credit

/**
 * @typedef {Object} CreateNodeOptions
 * @property {string} [class]
 * @property {(string | null)[]} [classes]
 * @property {string} [id]
 * @property {Node | string} [child]
 * @property {(Node | string | null)[]} [children]
 * @property {Object.<string, string>} [other] other attributes to add
 */

/**
 * Creates a new HTML node
 * @param {string} tag
 * @param {CreateNodeOptions} [options]
 * @returns {HTMLElement}
 */
function createNode(tag, options) {
	var node = document.createElement(tag);
	options = options || {};

	var classes = options.classes || [];
	if (options.class) { classes.push(options.class); }
	classes.forEach(function(cls) { if (cls) { node.classList.add(cls); } });

	var children = options.children || [];
	if (options.child) { children.push(options.child); }
	children.forEach(function(child) {
		if (typeof child === "string") {
			node.appendChild(document.createTextNode(child));
		}
		else if (child) {
			node.appendChild(child);
		}
	});

	if (options.id) {
		node.id = options.id;
	}

	var other = options.other || {};
	for (var name in other) {
		if (!Object.hasOwnProperty.call(other, name)) { continue; }
		if (!other[name]) { continue; }
		node.setAttribute(name, other[name]);
	}

	return node;
}

/** @returns {HTMLElement} */
function br() {
	return document.createElement("br");
}

/**
 * Sanitizes the given text to avoid issues while allowing mnemonic-safe tags through
 * @param {string} text the text to sanitize
 * @returns {HTMLElement}
 */
function sanitizeMnemonic(text) {
	function escapeHTML(text) {
		var map = {
			'&': '&amp;',
			'<': '&lt;',
			'>': '&gt;',
			'"': '&quot;',
			"'": '&#039;'
		};
		return text.replace(/[&<>"']/g, function(m) { return map[m]; });
	}

	// WK renamed these classes
	var renames = {
		'<span class="highlight-reading">': '<span class="reading-highlight">',
		'<span class="highlight-radical">': '<span class="radical-highlight">',
		'<span class="highlight-kanji">': '<span class="kanji-highlight">',
		'<span class="highlight-vocabulary">': '<span class="vocabulary-highlight">',
	};

	// matches <i>, </i>, etc, plus <span class="">
	var safeRegex = /<(?:\/?(?:br|p|i|u|s|b|em|strong|span|)|span class="[a-z\-]+") ?\/?>/g;

	var output = "";
	var currentIdx = 0;
	var match;
	while ((match = safeRegex.exec(text))) {
		output += escapeHTML(text.slice(currentIdx, match.index));
		if (renames[match[0]]) {
			output += renames[match[0]];
		}
		else {
			output += match[0];
		}
		currentIdx = safeRegex.lastIndex;
	}
	output += escapeHTML(text.slice(currentIdx));

	var node = document.createElement("span");
	node.innerHTML = output;
	return node;
}
/**
 * @param {string} string input string
 * @returns {ItemType | null}
 */
function itemTypeFromString(string) {
	switch (string) {
		case "r":
		case "radical":
			return ItemType.radical;
		case "k":
		case "kanji":
			return ItemType.kanji;
		case "v":
		case "vocabulary":
			return ItemType.vocabulary;
		default: return null;
	}
}
/**
 * @param {ItemType} itemType
 * @returns {boolean}
 */
function itemTypeSupportsMnemonics(itemType) {
	return itemType === ItemType.kanji || itemType === ItemType.vocabulary;
}
/**
 * Converts an item type to a one-character name used for differentiation
 * @param {ItemType} itemType
 * @return {?string}
 */
function itemTypeShortName(itemType) {
	switch (itemType) {
		case ItemType.radical: return "r";
		case ItemType.kanji: return "k";
		case ItemType.vocabulary: return "v";
		default: return null;
	}
}
/**
 * Converts an item type to a full word name like "kanji" or "vocabulary"
 * @param {ItemType} itemType
 * @return {?string}
 */
function itemTypeLongName(itemType) {
	switch (itemType) {
		case ItemType.radical: return "radical";
		case ItemType.kanji: return "kanji";
		case ItemType.vocabulary: return "vocabulary";
		default: return null;
	}
}
/**
 * Converts a mnemonic type to a one-character name used for differentiation
 * @param {MnemonicType} mnemonicType 
 * @returns {?string}
 */
function mnemonicTypeShortName(mnemonicType) {
	switch (mnemonicType) {
		case MnemonicType.meaning: return "m";
		case MnemonicType.reading: return "r";
		default: return null;
	}
}
/**
 * Converts a mnemonic type to a full word name like "meaning" or "reading"
 * @param {MnemonicType} mnemonicType 
 * @returns {?string}
 */
function mnemonicTypeLongName(mnemonicType) {
	switch (mnemonicType) {
		case MnemonicType.meaning: return "meaning";
		case MnemonicType.reading: return "reading";
		default: return null;
	}
}
/**
 * Makes a link to a WK user's page
 * @param {string} username The User's username
 * @returns {Node}
 */
function makeWKUserLink(username) {
	if (username === "c") {
		return document.createTextNode("a user whose name was lost by a bug in the old CM userscript");
	} else {
		var url = "https://www.wanikani.com/community/people/" + username;
		return createNode("a", {other: {href: url, target: "_blank"}, child: document.createTextNode(username)});
	}
}
/**
 * Convert columns from the spreadsheet into mnemonic objects
 * @param {string} name Name used for warning printouts
 * @param {string} mnemonicsColumn pipe-separated mnemonics
 * @param {string} usersColumn pipe-separated usernames
 * @param {string} scoresColumn pipe-separated scores
 * @returns {{submitted: Array<Mnemonic>, requested: Array<string>}}
 */
function itemsFromSpreadsheetColumns(name, mnemonicsColumn, usersColumn, scoresColumn) {
	var mnemonics = mnemonicsColumn.split("|");
	var users = usersColumn.split("|");
	var scores = scoresColumn.split("|");
	if (mnemonicsColumn.length === 0) { mnemonics = []; }
	if (usersColumn.length === 0) { users = []; }
	if (scoresColumn.length === 0) { scores = []; }
	if (mnemonicsColumn === "!") {
		return { submitted: [], requested: users };
	}
	var count = Math.min(mnemonics.length, users.length, scores.length);
	if (mnemonics.length !== count || users.length !== count || scores.length !== count) {
		console.log("Row " + name + " has mismatched items, there were " + mnemonics.length + " mnemonics, " + users.length + " users, and " + scores.length + " scores!");
	}
	/** @type Array<Mnemonic> */
	var output = [];
	for (var i = 0; i < count; i++) {
		output.push(new Mnemonic(mnemonics[i], users[i], Number(scores[i])));
	}
	return { submitted: output, requested: [] };
}
/**
 * @param {SpreadsheetRow} row
 */
function parseSpreadsheetRow(row) {
	if (row.item.length === 0) { return null; }
	var type = itemTypeFromString(row.item.substring(0, 1));
	var char = row.item.substring(1);
	var reading = itemsFromSpreadsheetColumns(row.item, row.readingmnem, row.readinguser, row.readingscore);
	var meaning = itemsFromSpreadsheetColumns(row.item, row.meaningmnem, row.meaninguser, row.meaningscore);
	return new ItemInfo(type, char, reading.submitted, meaning.submitted, reading.requested, meaning.requested);
}
/**
 * Finds the item with the given type and char or makes a new empty one if it doesn't exist
 * @param {ItemType} type - the type of entry (kanji or vocabulary)
 * @param {string} char - the word
 * @param {Object.<string, ItemInfo>} [objectToSearch=CMMainData.items] - the object to search for the items
 * @returns {ItemInfo}
 */
function findItem(type, char, objectToSearch) {
	if (objectToSearch === undefined) {
		objectToSearch = CMMainData.items;
	}
	var result = objectToSearch[itemTypeShortName(type) + char];
	if (result) {
		return result;
	}
	return new ItemInfo(type, char, [], [], [], []);
}
/**
 * Sort the mnemonics by their ratings
 * @param {Object.<string, ItemInfo>} [objectList=CMMainData.items]
 */
function sortMnemonicsByRating(objectList) {
	if (objectList === undefined) {
		objectList = CMMainData.items;
	}
	function comparator(a, b) {
		return b.votes - a.votes;
	}
	for (var key in objectList) {
		if (!Object.hasOwnProperty.call(objectList, key)) { continue; }
		var itemInfo = objectList[key];
		itemInfo.meaning = itemInfo.meaning.sort(comparator);
		itemInfo.reading = itemInfo.reading.sort(comparator);
	}
}
/**
 * Returns the first index of the given array where `check` returns true
 * Returns -1 if no item is found
 * @template T
 * @param {Array<T>} array 
 * @param {function(T): boolean} check 
 * @returns {number}
 */
function indexWhere(array, check) {
	for (var i = 0; i < array.length; i++) {
		if (check(array[i])) {
			return i;
		}
	}
	return -1;
}
/**
 * Maps the values of an object being used as a string -> Element dictionary
 * @template ValIn, ValOut
 * @param {Object.<string, ValIn>} object The object to transform
 * @param {function(string, ValIn): ?ValOut} transform A function used to transform the object.  Return `null` to remove the object completely
 * @returns {Object.<string, ValOut>}
 */
function compactMapValues(object, transform) {
	var out = {};
	for (var key in object) {
		if (!Object.hasOwnProperty.call(object, key)) { continue; }
		var val = object[key];
		var transformed = transform(key, val);
		if (transformed !== null) {
			out[key] = transformed;
		}
	}
	return out;
}

var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));
	o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!==64){
	t=t+String.fromCharCode(r);}if(a!==64){t=t+String.fromCharCode(i);}}t=Base64._utf8_decode(t);return t;},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){
	var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128);}else{t+=String.fromCharCode(r>>12|224);
	t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128);}}return t;},_utf8_decode:function(e){var t="";var n=0;var r=0,c1=0,c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){
	t+=String.fromCharCode(r);n++;}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2;}else{c2=e.charCodeAt(n+1);var c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|
	(c2&63)<<6|c3&63);n+=3;}}return t;}};
CMTableData = [];
CMTableItems = [];
CMData = {"k": [], "v": []};
CMSettings = {"k": [], "v": []};
CMVotes = {"k": [], "v": []};
CMPageIndex = {"m": 0, "r": 0};
CMSortMap = {"m": [], "r": []};
CMPageMap = {"m": [], "r": []};
CMLastTag = {"m": "", "r": ""};
CMInitData = true;
CMInitSettings = true;
CMInitVotes = true;
CMStylesAdded = false;
CMReady = false;
CMPostReady = true;
CMPreloadReady = false;
CMIndex = -1;
CMSelTemp = -1;
CMChar = "";
CMType = "";
CMUser = CMInfo.user;
CMVersionCheck = {"v": CMInfo.version, "c": false };
if (localStorage.getItem("CMVersionCheck") !== null) {
	CMVersionCheck = JSON.parse(localStorage.getItem("CMVersionCheck"));
	localStorage.setItem("CMVersionCheck", JSON.stringify(CMVersionCheck));
}
if (localStorage.getItem("CMData") !== null) {
		CMData = JSON.parse(localStorage.getItem("CMData"), function (key, value) {
			//Start Code Credit: Mike Samuel from Stack Overflow
			if (value && typeof value === 'object') {
				for (var k in value) {
					if (/^[A-Z]/.test(k) && Object.hasOwnProperty.call(value, k)) {
						value[encodeURIComponent(k)] = value[k];
						delete value[k];
					}
				}
			}
			return value;
			//End Code Credit
		});
	CMInitData = false;
}
if (localStorage.getItem("CMSettings") !== null) {
	CMSettings = JSON.parse(localStorage.getItem("CMSettings"), function (key, value) {
		//Start Code Credit: Mike Samuel from Stack Overflow
		if (value && typeof value === 'object') {
			for (var k in value) {
				if (/^[A-Z]/.test(k) && Object.hasOwnProperty.call(value, k)) {
					value[encodeURIComponent(k)] = value[k];
					delete value[k];
				}
			}
		}
		return value;
		//End Code Credit
	});
	CMInitSettings = false;
}
if (localStorage.getItem("CMVotes") !== null) {
	CMVotes = JSON.parse(localStorage.getItem("CMVotes"));
	CMInitVotes = false;
}
// ==/UserScript==